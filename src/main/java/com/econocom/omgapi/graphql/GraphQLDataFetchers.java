package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.io.IOException;

@Component
public class GraphQLDataFetchers {

    @Autowired
    OmgDataSource dataSource;

    public PersonFetcher person;
    public AuthentificationFetcher authentification;
    public ClientFetcher client;
    public MissionFetcher mission;
    public LocationFetcher location;
    public ApplicationFetcher application;
    public SkillFetcher skill;
    public CandidateFetcher candidate;

    @PostConstruct
    public void init() throws IOException {
        this.person = new PersonFetcher(dataSource);
        this.authentification = new AuthentificationFetcher(dataSource);
        this.client = new ClientFetcher(dataSource);
        this.mission = new MissionFetcher(dataSource);
        this.location = new LocationFetcher(dataSource);
        this.application = new ApplicationFetcher(dataSource);
        this.skill = new SkillFetcher(dataSource);
        this.candidate = new CandidateFetcher(dataSource);
    }
}
