package com.econocom.omgapi.graphql.scalar;

import graphql.schema.GraphQLScalarType;
import graphql.kickstart.servlet.apollo.ApolloScalars;

public class UploadScalarConfiguration {

    public static final GraphQLScalarType UPLOAD = ApolloScalars.Upload;

}
