package com.econocom.omgapi.graphql.scalar;

import graphql.language.StringValue;
import graphql.schema.*;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;

public class DateScalarConfiguration {

    public static final GraphQLScalarType DATE = new GraphQLScalarType("Date", "Java LocalDate as scalar.", new Coercing() {
        @Override
        public String serialize(final Object dataFetcherResult) {
            return dataFetcherResult.toString();
        }

        @Override
        public LocalDate parseValue(final Object input) {
            try {
                if (input instanceof String) {
                    return LocalDate.parse((String) input);
                } else {
                    throw new CoercingParseValueException("Expected a String");
                }
            } catch (DateTimeParseException e) {
                throw new CoercingParseValueException(String.format("Not a valid date: '%s'.", input), e
                );
            }
        }

        @Override
        public LocalDate parseLiteral(final Object input) {
            if (input instanceof StringValue) {
                try {
                    return LocalDate.parse(((StringValue) input).getValue());
                } catch (DateTimeParseException e) {
                    throw new CoercingParseLiteralException(e);
                }
            } else {
                throw new CoercingParseLiteralException("Expected a StringValue.");
            }
        }
    });
}
