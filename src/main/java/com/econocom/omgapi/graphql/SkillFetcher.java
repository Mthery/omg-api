package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;

import java.util.ArrayList;
import java.util.Map;

import static java.util.Map.entry;

public class SkillFetcher {

    OmgDataSource dataSource;

    public SkillFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Insert a new skill
     *
     * @return the id of the created row.
     */
    public DataFetcher createSkill() {
        return dataFetchingEnvironment -> {
            String[] fields = new String[]{dataFetchingEnvironment.getArgument("label")};

            String sqlQuery = new QueryBuilder()
                    .buildSimpleCreate(new String[]{"label"}, "skill")
                    .getSql();
            return dataSource.simpleCreate(sqlQuery, fields);
        };
    }

    /**
     * Insert new skills
     *
     * @return the id of the created row.
     */
    public DataFetcher createSkills() {
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<String> labels = dataFetchingEnvironment.getArgument("labels");

            labels.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleCreate(new String[]{"label"},
                                "skill")
                        .getSql();

                createdIds.add(dataSource.simpleCreate(sqlQuery, new String[]{obj}));
            });

            return createdIds;
        };
    }

    /**
     * Insert new skills for a mission
     *
     * @return the ids of the created rows.
     */
    public DataFetcher createMissionSkill() {
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<Map<String, Object>> createMissionSkill = dataFetchingEnvironment.getArgument("createMissionSkill");
            createMissionSkill.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleCreate(obj.keySet().toArray(new String[obj.size()]),
                                "mission_skill",
                                Map.ofEntries(entry("level", "level"))
                        )
                        .getSql();

                createdIds.add(dataSource.simpleCreate(sqlQuery, obj.values().toArray()));
            });

            return createdIds;
        };
    }

    /**
     * Delete skills linked to a mission
     *
     * @return a boolean if a row has been deleted.
     */
    public DataFetcher deleteMissionSkills() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("missionId");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleDelete("mission_skill", new String[]{"mission_id"})
                    .getSql();
            return dataSource.simpleDelete(sqlQuery, new Object[]{id});
        };
    }

    /**
     * Delete skills by id
     *
     * @return a list of boolean (true if a row has been deleted)
     */
    public DataFetcher deleteSkills() {
        return dataFetchingEnvironment -> {
            ArrayList<Boolean> deletedSuccessfully = new ArrayList<>();

            ArrayList<Integer> ids = dataFetchingEnvironment.getArgument("ids");

            ids.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleDelete("skill",
                                new String[]{"id"})
                        .getSql();

                deletedSuccessfully.add(dataSource.simpleDelete(sqlQuery, new Object[]{obj}));
            });

            return deletedSuccessfully;
        };
    }

    /**
     * Insert new skills for a person
     *
     * @return the ids of the created rows.
     */
    public DataFetcher createPersonSkill() {
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<Map<String, Object>> createPersonSkill = dataFetchingEnvironment.getArgument("createPersonSkill");
            createPersonSkill.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleCreate(obj.keySet().toArray(new String[obj.size()]),
                                "person_skill",
                                Map.ofEntries(entry("level", "level"))
                        )
                        .getSql();

                createdIds.add(dataSource.simpleCreate(sqlQuery, obj.values().toArray()));
            });

            return createdIds;
        };
    }

    /**
     * Delete skills linked to a person
     *
     * @return a boolean if a row has been deleted.
     */
    public DataFetcher deletePersonSkills() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("personId");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleDelete("person_skill", new String[]{"person_id"})
                    .getSql();
            return dataSource.simpleDelete(sqlQuery, new Object[]{id});
        };
    }

    /**
     * Query a single skill
     *
     * @return a skills.
     */
    public DataFetcher getSkill() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("id") ?
                    dataFetchingEnvironment.getArgument("id") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("skill_id");

            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "skill", new String[]{"id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
            }

            return null;
        };
    }

    /**
     * Query the entire list of skills
     *
     * @return a list of skills.
     */
    public DataFetcher getSkills() {
        return dataFetchingEnvironment -> {
            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "skill", null)
                    .getSql();
            return dataSource.simpleQuery(sqlQuery, null);
        };
    }

    /**
     * Query a list of skills for a given mission
     *
     * @return a list of missionSkills.
     */
    public DataFetcher getMissionSkillsByMission() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("missionId") ?
                    dataFetchingEnvironment.getArgument("missionId") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("id");

            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "mission_skill", new String[]{"mission_id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id});
            }

            return null;
        };
    }

    /**
     * Query a list of skills for a given skill
     *
     * @return a list of missionSkills.
     */
    public DataFetcher getMissionSkillsBySkill() {
        return dataFetchingEnvironment -> {
            Integer skillId = (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("id");

            if (skillId != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "mission_skill", new String[]{"skill_id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{skillId});
            }

            return null;
        };
    }

    /**
     * Update existing skills
     *
     * @return number of lines updated
     */
    public DataFetcher updateSkills() {
        return dataFetchingEnvironment -> {
            final Integer[] updated = {0};

            ArrayList<Map<String, Object>> skills = dataFetchingEnvironment.getArgument("skills");
            skills.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleUpdate(new String[] {"label"},
                                "skill")
                        .getSql();

                Object[] param = new Object[] {obj.get("label"), obj.get("id")};

                updated[0] += dataSource.simpleUpdate(sqlQuery, param);
            });

            return updated[0];
        };
    }

    /**
     * Query a list of skills for a given person
     *
     * @return a list of skills.
     */
    public DataFetcher getPersonSkills() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("personId") ?
                    dataFetchingEnvironment.getArgument("personId") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("id");

            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "person_skill", new String[]{"person_id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id});
            }

            return null;
        };
    }

}
