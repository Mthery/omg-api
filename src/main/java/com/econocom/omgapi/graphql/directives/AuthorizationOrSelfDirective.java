package com.econocom.omgapi.graphql.directives;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.token.Token;
import graphql.kickstart.servlet.context.GraphQLServletContext;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingEnvironment;
import graphql.schema.GraphQLFieldDefinition;
import graphql.schema.GraphQLFieldsContainer;
import graphql.schema.idl.SchemaDirectiveWiring;
import graphql.schema.idl.SchemaDirectiveWiringEnvironment;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.ArrayList;

public class AuthorizationOrSelfDirective implements SchemaDirectiveWiring {

    @Autowired
    OmgDataSource dataSource;

    @Override
    public GraphQLFieldDefinition onField(SchemaDirectiveWiringEnvironment<GraphQLFieldDefinition> environment) {
        ArrayList<String> authorizationGroup = (ArrayList<String>) environment.getDirective().getArgument("profil").getValue();
        GraphQLFieldDefinition field = environment.getElement();
        GraphQLFieldsContainer parentType = environment.getFieldsContainer();

        DataFetcher originalDataFetcher = environment.getCodeRegistry().getDataFetcher(parentType, field);
        DataFetcher authDataFetcher = new DataFetcher() {
            @Override
            public Object get(DataFetchingEnvironment dataFetchingEnvironment) throws Exception {
                Token token = new Token(((GraphQLServletContext) dataFetchingEnvironment.getContext()).getHttpServletRequest().getHeader("Authorization"));
                if (authorizationGroup.contains(token.getUserRole())) {
                    return originalDataFetcher.get(dataFetchingEnvironment);
                } else {
                    if (dataFetchingEnvironment.containsArgument("id") && dataFetchingEnvironment.getFieldDefinition().getName().equals("person")) {
                        Integer id = dataFetchingEnvironment.getArgument("id");
                        if (id == token.getUserId()) {
                            return originalDataFetcher.get(dataFetchingEnvironment);
                        } else {
                            throw new Exception("Wrong access.");
                        }
                    } else {
                        throw new Exception("Wrong access.");
                    }
                }
            }
        };
        environment.getCodeRegistry().dataFetcher(parentType, field, authDataFetcher);
        return field;
    }

}
