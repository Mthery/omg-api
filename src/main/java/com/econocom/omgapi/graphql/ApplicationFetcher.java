package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.util.ArrayList;
import java.util.Map;
import static java.util.Map.entry;

public class ApplicationFetcher {

    OmgDataSource dataSource;

    public ApplicationFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataFetcher addMissionCandidate() {
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<Map<String, Object>> addMissionCandidate = dataFetchingEnvironment.getArgument("addMissionCandidate");
            addMissionCandidate.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleCreate(obj.keySet().toArray(new String[obj.size()]),
                                "mission_candidate")
                        .getSql();

                createdIds.add(dataSource.simpleCreate(sqlQuery, obj.values().toArray()));
            });

            return createdIds;
        };
    }

    public DataFetcher removeMissionCandidate() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("missionId");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleDelete("mission_candidate", new String[] {"mission_id"})
                    .getSql();
            return dataSource.simpleDelete(sqlQuery, new Object[] {id});
        };
    }
}

