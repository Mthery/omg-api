package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import com.econocom.omgapi.models.Collaborator;
import com.econocom.omgapi.models.Level;
import com.econocom.omgapi.models.Skill;
import graphql.schema.DataFetcher;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class CandidateFetcher {
    OmgDataSource dataSource;

    public CandidateFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Determine a list of candidates by comparing their skills and the mission' requirements
     * @return a list of candidates.
     */
    public DataFetcher getCandidates() {
        return dataFetchingEnvironment -> {
            List<Map<String, Object>> skills = dataFetchingEnvironment.getArgument("skills");

            List<Map<String, Object>> mandatorySkills = new ArrayList<>();
            List<Map<String, Object>> optionalSkills = new ArrayList<>();

            for (Map<String, Object> ms : skills) {
                if ((boolean) ms.get("mandatory"))
                    mandatorySkills.add(ms);
                else
                    optionalSkills.add(ms);
            }
            Integer skillCount = mandatorySkills.size() + optionalSkills.size();

            List<Collaborator> collaborators = this.getCollaborators();
            List<Collaborator> filteredCollab = this.filterCollabByRequirements(mandatorySkills, optionalSkills, collaborators);
            this.computateMatchingPercentage(filteredCollab, skillCount);

            return filteredCollab.size() > 0 ? this.getResult(filteredCollab) : null;
        };
    }

    /**
     * Check if the collaborator knows the required skill with a matching level
     * @param collaborator Collaborator we check
     * @param skill Required skill
     * @return True if the collaborator match, false otherwise
     */
    private boolean collabHasSkillAndLevel(Collaborator collaborator, Map<String, Object> skill) {
        Skill s = new Skill((Integer) skill.get("skill_id"), (String) skill.get("level"));

        if (collaborator.getSkills().contains(s)) {
            Integer i = collaborator.getSkills().indexOf(s);
            Level collabSkillLevel = this.getLevel(collaborator.getSkills().get(i).getLevel());
            Level skillLevel = this.getLevel(s.getLevel());

            return this.isLevelEnough(skillLevel, collabSkillLevel);
        }

        return false;
    }

    /**
     * Computate the percentage of match between each candidates skills and the missions' requirements
     * @param collaborators List of candidates
     * @param skillCount Number of skills linked to the mission
     */
    private void computateMatchingPercentage(List<Collaborator> collaborators, Integer skillCount) {
        for (Collaborator collaborator : collaborators) {
            collaborator.setPercentage((collaborator.getSkills().size() * 100) / skillCount);
            if (collaborator.getPercentage() > 100)
                collaborator.setPercentage(100);
        }
    }

    /**
     * Create a list of Collaborator from the result of a query
     * @param collabSkills Result of a query (with collaborator id, skill id and skill level)
     * @return List of Collaborator (with id and list of skill ids with their respective levels)
     */
    private List<Collaborator> createListCollaborator(List<Map<String, Object>> collabSkills) {
        List<Collaborator> collaborators = new ArrayList<>();

        for (Map<String, Object> cs : collabSkills) {
            Collaborator tmpCollab = new Collaborator((Integer) cs.get("id"));

            if (collaborators.contains(tmpCollab)) {
                Integer i = collaborators.indexOf(tmpCollab);
                collaborators.get(i).addSkill(new Skill((Integer) cs.get("skill_id"), (String) cs.get("level")));
            } else {
                List<Skill> skills = new ArrayList<>();
                skills.add(new Skill((Integer) cs.get("skill_id"), (String) cs.get("level")));
                collaborators.add(new Collaborator((Integer) cs.get("id"), skills));
            }
        }

        return collaborators;
    }

    /**
     * Method used to filter collaborators depending on their skills and the requirements
     * @param mandatorySkills Mandatory skills list (has priority)
     * @param optionalSkills Optional skills list (used when there is no mandatory skills)
     * @param collaborators Collaborators list
     * @return Collaborators list filtered by skills
     */
    private List<Collaborator> filterCollabByRequirements(List<Map<String, Object>> mandatorySkills,
                                                          List<Map<String, Object>> optionalSkills,
                                                          List<Collaborator> collaborators) {
        List<Collaborator> filterCollabSkills = new ArrayList<>();

        // If there's only one mandatory skill, collaborators must know it
        if (mandatorySkills.size() == 1) {
            for (Collaborator collab : collaborators) {
                if (this.collabHasSkillAndLevel(collab, mandatorySkills.get(0))) {
                    filterCollabSkills.add(collab);
                }
            }
        } else {
            // If there's more than one mandatory skill, collaborators must know at least half of it (rounded up)
            if (mandatorySkills.size() > 1) {
                Integer nbSkillsToHave = Math.round((float) mandatorySkills.size() / 2);

                for (Collaborator collab : collaborators) {
                    Integer nbSkillsCollab = 0;

                    for (Map<String, Object> skill : mandatorySkills) {
                        if (this.collabHasSkillAndLevel(collab, skill)) {
                            nbSkillsCollab++;

                            if (nbSkillsCollab >= nbSkillsToHave) {
                                filterCollabSkills.add(collab);
                                break;
                            }
                        }
                    }
                }
            } else {
                // If there's no mandatory skills, Collaborators must know at least one optional skill
                if (optionalSkills.size() >= 1) {
                    for (Collaborator collab : collaborators) {
                        for (Map<String, Object> skill : optionalSkills) {
                            if (this.collabHasSkillAndLevel(collab, skill)) {
                                filterCollabSkills.add(collab);
                                break;
                            }
                        }
                    }
                }
            }
        }

        return filterCollabSkills;
    }

    /**
     * Get collaborators and skills from database, then create a list
     * @return List of collaborators and their known skills
     */
    private List<Collaborator> getCollaborators() {
        String sqlQueryCollab = new QueryBuilder()
                .buildJoinQuery(new String[]{"c.id", "ps.skill_id", "ps.level"},
                        "collaborator c",
                        new String[]{"person_skill ps"},
                        new String[]{"c.id = ps.person_id"},
                        null)
                .getSql();
        List<Map<String, Object>> collabSkills = dataSource.simpleQuery(sqlQueryCollab, null);

        return this.createListCollaborator(collabSkills);
    }

    /**
     * Transform a database Skill into a Level (enum)
     * @param value Database Skill
     * @return Corresponding Level (or null)
     */
    private Level getLevel(String value) {
        for (Level l : Level.values()) {
            if (l.getValue().equals(value))
                return l;
        }

        return null;
    }

    /**
     * Get a list of Person (in database) from a Collaborator list
     * @param collaborators Collaborator list (only the id is useful here)
     * @return Person list
     */
    private List<Map<String, Object>> getResult(List<Collaborator> collaborators) {
        List<String> ids = new ArrayList<>();

        for (Collaborator collaborator : collaborators) {
            if (!ids.contains(collaborator.getId()))
                ids.add(collaborator.getId().toString());
        }

        String sqlQuery = new QueryBuilder()
                .buildSimpleIntervalQuery(null,
                        "person",
                        "id",
                        ids.toArray(new String[ids.size()]))
                .getSql();

        List<Map<String, Object>> result = dataSource.simpleQuery(sqlQuery, null);
        for (Map<String, Object> candidate : result) {
            Integer percentage = collaborators.stream()
                    .filter(collab -> collab.getId() == candidate.get("id"))
                    .findFirst()
                    .get()
                    .getPercentage();
            candidate.put("percentage", percentage);
        }

        return result;
    }

    /**
     * Determine if current level is enough compared to goal level
     * @param goal Goal level
     * @param current Current level
     * @return true if goal - 1 <= current <= goal + 1, false otherwise
     */
    private boolean isLevelEnough(Level goal, Level current) {
        return goal.getKey() - 1 <= current.getKey() && current.getKey() <= goal.getKey() + 1;
    }
}
