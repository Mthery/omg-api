package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;

import java.util.ArrayList;
import java.util.Map;

public class ClientFetcher {

    OmgDataSource dataSource;

    public ClientFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataFetcher createClient(){
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("createClient");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleCreate(fields.keySet().toArray(new String[0]), "client")
                    .getSql();
            return dataSource.simpleCreate(sqlQuery, fields.values().toArray());
        };
    }

    public DataFetcher createClientsWithLocations(){
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<Map<String, Object>> clients = dataFetchingEnvironment.getArgument("createClientWithLocation");

            clients.forEach(obj -> {
                String sqlQueryCreateClient = new QueryBuilder()
                        .buildSimpleCreate(new String[]{"label"},
                                "client")
                        .getSql();

                String[] valuesClient = new String[]{obj.get("label").toString()};
                Integer id = dataSource.simpleCreate(sqlQueryCreateClient, valuesClient);
                createdIds.add(id);

                ArrayList<Map<String, Object>> locations = (ArrayList<Map<String, Object>>) obj.get("location");

                locations.forEach(l -> {
                    String sqlQueryCreateLocation = new QueryBuilder()
                            .buildSimpleCreate(new String[]{"client_id", "label"},
                                    "client_location")
                            .getSql();

                    Object[] valuesLocation = new Object[]{id, l.get("label")};
                    dataSource.simpleCreate(sqlQueryCreateLocation, valuesLocation);
                });
            });

            return createdIds;
        };
    }

    /**
     * Delete clients by id
     *
     * @return a list of boolean (true if a row has been deleted)
     */
    public DataFetcher deleteClients() {
        return dataFetchingEnvironment -> {
            ArrayList<Boolean> deletedSuccessfully = new ArrayList<>();

            ArrayList<Integer> ids = dataFetchingEnvironment.getArgument("ids");

            ids.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleDelete("client",
                                new String[]{"id"})
                        .getSql();

                deletedSuccessfully.add(dataSource.simpleDelete(sqlQuery, new Object[]{obj}));
            });

            return deletedSuccessfully;
        };
    }

    /**
     * Query a single client.
     *
     * @return a client's row.
     */
    public DataFetcher getClient() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("id") ?
                    dataFetchingEnvironment.getArgument("id") :
                    ((Map<String, Object>) dataFetchingEnvironment.getSource()).containsKey("client_id") ?
                            (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("client_id") :
                            null;
            if (id == null) {
                return null;
            }
            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "client", new String[]{"id"})
                    .getSql();
            return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
        };
    }

    /**
     * Query the entire list of clients
     *
     * @return a list of clients.
     */
    public DataFetcher getClients() {
        return dataFetchingEnvironment -> {
            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "client", null)
                    .getSql();
            return dataSource.simpleQuery(sqlQuery, null);
        };
    }

    /**
     * Query a single Client from a given location
     *
     * @return a client's row.
     */
    public DataFetcher getClientFromLocation() {
        return dataFetchingEnvironment -> {
            Integer locationId = ((Map<String, Object>) dataFetchingEnvironment.getSource()).containsKey("client_location_id") ?
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("client_location_id") :
                    null;
            if (locationId == null) return null;

            String sqlQueryLocation = new QueryBuilder()
                    .buildSimpleQuery(new String[]{"client_id"}, "client_location", new String[]{"id"})
                    .getSql();
            Integer id = (int) dataSource.simpleQuery(
                    sqlQueryLocation,
                    new Object[]{locationId}
            ).get(0).get("client_id");

            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "client", new String[]{"id"})
                    .getSql();
            return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
        };
    }

    /**
     * Update existing clients
     *
     * @return number of lines updated
     */
    public DataFetcher updateClients() {
        return dataFetchingEnvironment -> {
            final Integer[] updated = {0};

            ArrayList<Map<String, Object>> clients = dataFetchingEnvironment.getArgument("clients");
            clients.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleUpdate(new String[] {"label"},
                                "client")
                        .getSql();

                Object[] param = new Object[] {obj.get("label"), obj.get("id")};

                updated[0] += dataSource.simpleUpdate(sqlQuery, param);
            });

            return updated[0];
        };
    }

    /**
     * Update a client and its locations
     * Starts by deleting its locations in database
     * Then creates them back and updates the client's label
     *
     * @return number of updated rows (always 1 here)
     */
    public DataFetcher updateClientWithLocation() {
        return dataFetchingEnvironment -> {
            Map<String, Object> clientWithLocation = dataFetchingEnvironment.getArgument("updateClientWithLocation");
            ArrayList<Map<String, Object>> locations = (ArrayList<Map<String, Object>>) clientWithLocation.get("location");
            Integer clientId = (Integer) clientWithLocation.get("id");

            String sqlQueryDeleteLocations = new QueryBuilder()
                    .buildSimpleDelete("client_location",
                            new String[]{ "client_id" })
                    .getSql();
            dataSource.simpleDelete(sqlQueryDeleteLocations, new Object[]{ clientId });

            locations.forEach(loc -> {
                String sqlQueryCreateLocation = new QueryBuilder()
                        .buildSimpleCreate(new String[]{ "label", "client_id" }, "client_location")
                        .getSql();
                dataSource.simpleCreate(sqlQueryCreateLocation, new Object[]{ loc.get("label"), clientId });
            });

            String sqlQueryUpdateClient = new QueryBuilder()
                    .buildSimpleUpdate(new String[]{ "label" }, "client")
                    .getSql();
            return dataSource.simpleUpdate(sqlQueryUpdateClient, new Object[]{ clientWithLocation.get("label"), clientId });
        };
    }
}
