package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.util.*;

import static java.util.Map.entry;

public class PersonFetcher {
    OmgDataSource dataSource;

    public PersonFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Query the entire list of person
     *
     * @return a list of person.
     */
    public DataFetcher getPersons() {
        return dataFetchingEnvironment -> {
            Map<String, Object> options = dataFetchingEnvironment.getArgument("options");
            boolean hasFilter = options != null && options.containsKey("searchFilter") && !options.get("searchFilter").toString().isEmpty();
            boolean sortFilter = options != null && options.containsKey("sortFieldFilter") && !options.get("sortFieldFilter").toString().isEmpty();

            String sortField = (sortFilter ?
                    options.get("sortFieldFilter").toString() :
                    ""
            );
            String sortDirection = (sortFilter ?
                    options.get("sortDirectionFilter").toString() :
                    ""
            );
            DataFetchingFieldSelectionSet sortSet = dataFetchingEnvironment.getSelectionSet();

            String sqlQuery = new QueryBuilder()
                    .buildJoinQuery(null, "person", new String[]{
                            "collaborator"
                    }, new String[]{
                            "collaborator.id = person.id"
                    }, null, "LEFT")
                    .addSearchRegex(hasFilter)
                    .addSortFilter(sortFilter, sortField, sortDirection, sortSet)
                    .getSql();
            Object[] parameters = (hasFilter ?
                    (((String) options.get("searchFilter")).length() > 0 ?
                            new Object[]{"%" + options.get("searchFilter") + "%"} :
                            null) :
                    null);
            return dataSource.simpleQuery(sqlQuery, parameters);
        };
    }

    /**
     * Query a single person
     *
     * @return a person's row.
     */
    public DataFetcher getPerson() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("id") ?
                    dataFetchingEnvironment.getArgument("id") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get(dataFetchingEnvironment.getField().getName());
            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildJoinQuery(null, "person", new String[]{
                                "collaborator"
                        }, new String[]{
                                "collaborator.id = person.id"
                        }, new String[]{"person.id"}, "LEFT")
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
            }
            return null;
        };
    }

    /**
     * Query a single person by its name (lastname + firstname)
     *
     * @return a person's row
     */
    public DataFetcher getPersonByName() {
        return dataFetchingEnvironment -> {
            String lastname = dataFetchingEnvironment.getArgument("lastname");
            String firstname = dataFetchingEnvironment.getArgument("firstname");

            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null,
                            "person",
                            new String[] {"lastname", "firstname"})
                    .getSql();

            List<Map<String, Object>> result = dataSource.simpleQuery(sqlQuery, new Object[] {lastname, firstname});
            return result.size() > 0 ? result.get(0) : null;
        };
    }

    /**
     * Query all collaborators (as persons)
     *
     * @return all collaborators.
     */
    public DataFetcher getCollaborators() {
        return dataFetchingEnvironment -> {
            String sqlQuery = new QueryBuilder()
                .buildJoinQuery(null, "collaborator",
                        new String[]{ "person" },
                        new String[]{ "person.id = collaborator.id" },
                        null,
                        "INNER")
                .getSql();
            return dataSource.simpleQuery(sqlQuery, null);
        };
    }

    /**
     * Query a single person (as manager)
     *
     * @return a person's row.
     */
    public DataFetcher getManager() {
        return dataFetchingEnvironment -> {
            Integer id = (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("manager_id");
            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "person", new String[]{"id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
            }
            return null;
        };
    }

    public DataFetcher getMissionCandidatesFrom() {
        return dataFetchingEnvironment -> {
            Integer missionId = (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("id");

            if (missionId != null) {
                String sqlQueryMissionCandidates = new QueryBuilder()
                        .buildSimpleQuery(null, "mission_candidate", new String[]{"mission_id"})
                        .getSql();
                List<Map<String, Object>> missionCandidates = dataSource.simpleQuery(sqlQueryMissionCandidates, new Object[]{missionId});
                List<Integer> collaboratorIds = new ArrayList<>();
                for (Map<String, Object> mc: missionCandidates) {
                    collaboratorIds.add((Integer) mc.get("collaborator_id"));
                }

                List<Map<String, Object>> candidates = new ArrayList<>();
                for (Integer collaboratorId: collaboratorIds) {
                    String sqlQueryCandidates = new QueryBuilder()
                            .buildSimpleQuery(null, "person", new String[]{"id"})
                            .getSql();
                    candidates.add(dataSource.simpleQuery(sqlQueryCandidates, new Object[]{collaboratorId}).get(0));
                }
                return candidates;
            }

            return null;
        };
    }

    /**
     * Insert a new person
     *
     * @return the id of the created row.
     */
    public DataFetcher createPerson() {
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("createPerson");
            Map<String, Object> subfields = new HashMap<>();
            boolean hasOccupation = false;

            String profil = ((String) fields.get("profil"));

            // Remove subfields
            switch (profil) {
                case "INTERN":
                case "FREELANCE":
                case "CONTRACTOR":
                case "CANDIDATE":
                    subfields = (Map<String, Object>) fields.remove("collaborator");
                    if (subfields == null) {
                        throw new Exception("Missing arguments - Collaborator");
                    }
                    break;
            }

            // Create a person
            String sqlPerson = new QueryBuilder()
                    .buildSimpleCreate(
                            fields.keySet().toArray(new String[fields.size()]),
                            "person",
                            Map.ofEntries(entry("profil", "person_profil"))
                    )
                    .getSql();
            int id = dataSource.simpleCreate(sqlPerson, fields.values().toArray());

            // Copy the newly created ID
            subfields.put("id", id);

            // Create a collaborator
            switch (profil) {
                case "INTERN":
                case "FREELANCE":
                case "CONTRACTOR":
                case "CANDIDATE":
                    // Create collaborator
                    String sqlCollaborator = new QueryBuilder()
                            .buildSimpleCreate(
                                    subfields.keySet().toArray(new String[subfields.size()]),
                                    "collaborator"
                            )
                            .getSql();
                    dataSource.simpleCreate(sqlCollaborator, subfields.values().toArray());
                    break;
            }
            return id;
        };
    }

    /**
     * Update a person
     *
     * @return a boolean if a row has been updated.
     */
    public DataFetcher editPerson() {
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("editPerson");
            Integer id = dataFetchingEnvironment.getArgument("id");

            if (fields != null) {
                Map<String, Object> subfields = new HashMap<>();
                boolean hasOccupation = false;

                String profil = ((String) fields.get("profil"));
                if (profil == null) {
                    String sqlProfil = new QueryBuilder()
                            .buildSimpleQuery(new String[]{"profil"}, "person", new String[]{"id"})
                            .getSql();
                    profil = (String) dataSource.simpleQuery(sqlProfil, new Object[]{id}).get(0).get("profil");
                }

                // Remove subfields
                switch (profil) {
                    case "INTERN":
                    case "FREELANCE":
                    case "CONTRACTOR":
                    case "CANDIDATE":
                        subfields = (Map<String, Object>) fields.remove("collaborator");
                        break;
                }

                // Update a person
                if (!fields.isEmpty()) {
                    String sqlPerson = new QueryBuilder()
                            .buildSimpleUpdate(
                                    fields.keySet().toArray(new String[fields.size()]),
                                    "person",
                                    Map.ofEntries(entry("profil", "person_profil"))
                            )
                            .getSql();
                    Object[] values = fields.values().toArray();
                    Object[] param = new Object[values.length + 1];
                    System.arraycopy(values, 0, param, 0, values.length);
                    param[values.length] = id;
                    dataSource.simpleUpdate(sqlPerson, param);
                }

                // Create a collaborator
                if (subfields != null) {
                    switch (profil) {
                        case "INTERN":
                        case "FREELANCE":
                        case "CONTRACTOR":
                        case "CANDIDATE":
                            // Update collaborator
                            if (!subfields.isEmpty()) {
                                String sqlCollaborator = new QueryBuilder()
                                        .buildSimpleUpdate(
                                                subfields.keySet().toArray(new String[subfields.size()]),
                                                "collaborator"
                                        )
                                        .getSql();
                                Object[] values = subfields.values().toArray();
                                Object[] param = new Object[values.length + 1];
                                System.arraycopy(values, 0, param, 0, values.length);
                                param[values.length] = id;
                                dataSource.simpleUpdate(sqlCollaborator, param);
                            }
                            break;
                    }
                }
            }
            return id;
        };
    }

    /**
     * Delete a person
     *
     * @return a boolean if a row has been deleted.
     */
    public DataFetcher deletePerson() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("id");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleDelete("person", new String[]{"id"})
                    .getSql();
            return dataSource.simpleDelete(sqlQuery, new Object[]{id});
        };
    }

    public DataFetcher editProfils() {
        return dataFetchingEnvironment -> {
            ArrayList<Map<String, Object>> editProfils = dataFetchingEnvironment.getArgument("editProfils");
            final Integer[] updated = {0};

            editProfils.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleUpdate(new String[]{"profil"},
                                "person",
                                Map.ofEntries(entry("profil", "person_profil")))
                        .getSql();
                Object[] params = new Object[]{obj.get("profil"), obj.get("id")};

                updated[0] += dataSource.simpleUpdate(sqlQuery, params);
            });

            return updated[0];
        };
    }
}
