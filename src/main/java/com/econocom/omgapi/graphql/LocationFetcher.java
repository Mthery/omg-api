package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;

import java.util.ArrayList;
import java.util.Map;

public class LocationFetcher {

    OmgDataSource dataSource;

    public LocationFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataFetcher createLocation() {
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("createLocation");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleCreate(fields.keySet().toArray(new String[0]), "client_location")
                    .getSql();
            return dataSource.simpleCreate(sqlQuery, fields.values().toArray());
        };
    }

    public DataFetcher createLocations() {
        return dataFetchingEnvironment -> {
            ArrayList<Integer> createdIds = new ArrayList<>();

            ArrayList<Map<String, Object>> locations = dataFetchingEnvironment.getArgument("createLocations");

            locations.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleCreate(obj.keySet().toArray(new String[0]),
                                "client_location")
                        .getSql();

                createdIds.add(dataSource.simpleCreate(sqlQuery, obj.values().toArray()));
            });

            return createdIds;
        };
    }

    /**
     * Delete locations by id
     *
     * @return a list of boolean (true if a row has been deleted)
     */
    public DataFetcher deleteLocations() {
        return dataFetchingEnvironment -> {
            ArrayList<Boolean> deletedSuccessfully = new ArrayList<>();

            ArrayList<Integer> ids = dataFetchingEnvironment.getArgument("ids");

            ids.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleDelete("client_location",
                                new String[]{"id"})
                        .getSql();

                deletedSuccessfully.add(dataSource.simpleDelete(sqlQuery, new Object[]{obj}));
            });

            return deletedSuccessfully;
        };
    }

    /**
     * Query a single location.
     *
     * @return a location's row.
     */
    public DataFetcher getLocation() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("id") ?
                    dataFetchingEnvironment.getArgument("id") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("client_location_id");
            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "client_location", new String[]{"id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
            }
            return null;
        };
    }

    /**
     * Query the entire list of locations
     *
     * @return a list of locations.
     */
    public DataFetcher getLocations() {
        return dataFetchingEnvironment -> {
            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "client_location", null)
                    .getSql();
            return dataSource.simpleQuery(sqlQuery, null);
        };
    }

    /**
     * Update existing locations
     *
     * @return number of lines updated
     */
    public DataFetcher updateLocations() {
        return dataFetchingEnvironment -> {
            final Integer[] updated = {0};

            ArrayList<Map<String, Object>> locations = dataFetchingEnvironment.getArgument("locations");
            locations.forEach(obj -> {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleUpdate(new String[] {"label"},
                                "client_location")
                        .getSql();

                Object[] param = new Object[] {obj.get("label"), obj.get("id")};

                updated[0] += dataSource.simpleUpdate(sqlQuery, param);
            });

            return updated[0];
        };
    }
}
