package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;
import graphql.schema.DataFetchingFieldSelectionSet;

import java.util.Collection;
import java.util.Map;
import java.util.Set;

import static java.util.Map.entry;

public class MissionFetcher {
    OmgDataSource dataSource;

    public MissionFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Query the entire list of mission
     *
     * @return a list of mission.
     */
    public DataFetcher getMissions() {
        return dataFetchingEnvironment -> {
            Map<String, Object> options = dataFetchingEnvironment.getArgument("options");
            boolean hasFilter = options != null && options.containsKey("searchFilter") && !options.get("searchFilter").toString().isEmpty();
            boolean sortFilter = options != null && options.containsKey("sortFieldFilter") && !options.get("sortFieldFilter").toString().isEmpty();

            String sortField = (sortFilter ?
                    options.get("sortFieldFilter").toString() :
                    ""
            );
            String sortDirection = (sortFilter ?
                    options.get("sortDirectionFilter").toString() :
                    ""
            );
            DataFetchingFieldSelectionSet sortSet = dataFetchingEnvironment.getSelectionSet();
            String sqlQuery = new QueryBuilder()
                    .buildSimpleQuery(null, "mission", null)
                    .addSearchRegex(hasFilter)
                    .addSortFilter(sortFilter, sortField, sortDirection, sortSet)
                    .getSql();
            Object[] parameters = (hasFilter ?
                    (((String) options.get("searchFilter")).length() > 0 ?
                            new Object[]{"%" + options.get("searchFilter") + "%"} :
                            null) :
                    null);
            return dataSource.simpleQuery(sqlQuery, parameters);
        };
    }

    /**
     * Query a single mission
     *
     * @return a mission's row.
     */
    public DataFetcher getMission() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.containsArgument("id") ?
                    dataFetchingEnvironment.getArgument("id") :
                    (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("mission_id");
            if (id != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "mission", new String[]{"id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{id}).get(0);
            }
            return null;
        };
    }

    public DataFetcher getMissionsByClientLocation() {
        return dataFetchingEnvironment -> {
            Integer ClientLocationId = (Integer) ((Map<String, Object>) dataFetchingEnvironment.getSource()).get("id");

            if (ClientLocationId != null) {
                String sqlQuery = new QueryBuilder()
                        .buildSimpleQuery(null, "mission", new String[]{"client_location_id"})
                        .getSql();
                return dataSource.simpleQuery(sqlQuery, new Object[]{ClientLocationId});
            }

            return null;
        };
    }

    /**
     * Insert a new mission
     *
     * @return the id of the created row.
     */
    public DataFetcher createMission() {
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("createMission");

            String sqlQuery = new QueryBuilder()
                    .buildSimpleCreate(fields.keySet().toArray(new String[0]), "mission",
                            Map.ofEntries(entry("status", "mission_status"),
                                    entry("priority", "mission_priority"),
                                    entry("type", "mission_type"),
                                    entry("agency", "mission_agency")))
                    .getSql();
            int id = dataSource.simpleCreate(sqlQuery, fields.values().toArray());
            return id;
        };
    }

    /**
     * Duplicate a mission based on its id (only data in mission table, not in mission_skill or mission_candidate)
     *
     * @return id of the new mission
     */
    public DataFetcher duplicateMission() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("id");
            Integer createdBy = dataFetchingEnvironment.getArgument("createdBy");

            String sqlQueryGetMission = new QueryBuilder()
                    .buildSimpleQuery(null,
                            "mission",
                            new String[]{"id"})
                    .getSql();
            Map<String, Object> missionToDuplicate = dataSource.simpleQuery(sqlQueryGetMission, new Object[]{id}).get(0);
            missionToDuplicate.remove("id");
            missionToDuplicate.put("created_by", createdBy);

            Set fields = missionToDuplicate.keySet();
            Collection<Object> values = missionToDuplicate.values();

            String sqlQueryCreateMission = new QueryBuilder()
                    .buildSimpleCreate((String[]) fields.toArray(new String[0]), "mission",
                            Map.ofEntries(entry("status", "mission_status"),
                                    entry("priority", "mission_priority"),
                                    entry("type", "mission_type"),
                                    entry("agency", "mission_agency")))
                    .getSql();

            return dataSource.simpleCreate(sqlQueryCreateMission, values.toArray());
        };
    }

    /**
     * Edit a mission
     *
     * @return the number of edited rows.
     */
    public DataFetcher editMission() {
        return dataFetchingEnvironment -> {
            Map<String, Object> fields = dataFetchingEnvironment.getArgument("editMission");
            Integer id = dataFetchingEnvironment.getArgument("id");

            // Get every values as array, and insert the id at the end
            String[] field = (fields.keySet()).toArray(new String[0]);
            Object[] value = (fields.values()).toArray();

            Object[] param = new Object[value.length + 1];
            System.arraycopy(value, 0, param, 0, value.length);
            param[value.length] = id;

            String sqlQuery = new QueryBuilder()
                    .buildSimpleUpdate(field, "mission",
                            Map.ofEntries(entry("status", "mission_status"),
                                    entry("priority", "mission_priority"),
                                    entry("type", "mission_type"),
                                    entry("agency", "mission_agency")))
                    .getSql();
            int row = dataSource.simpleUpdate(sqlQuery, param);
            return row;
        };
    }

    /**
     * Delete a mission
     *
     * @return a boolean if a row has been deleted.
     */
    public DataFetcher deleteMission() {
        return dataFetchingEnvironment -> {
            Integer id = dataFetchingEnvironment.getArgument("id");
            String sqlQuery = new QueryBuilder()
                    .buildSimpleDelete("mission", new String[] {"id"})
                    .getSql();
            return dataSource.simpleDelete(sqlQuery, new Object[] {id});
        };
    }
}
