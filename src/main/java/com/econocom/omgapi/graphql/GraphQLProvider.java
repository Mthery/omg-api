package com.econocom.omgapi.graphql;

import com.econocom.omgapi.graphql.directives.AuthorizationDirective;
import com.econocom.omgapi.graphql.directives.AuthorizationOrSelfDirective;
import com.econocom.omgapi.graphql.scalar.DateScalarConfiguration;
import com.econocom.omgapi.graphql.scalar.UploadScalarConfiguration;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import graphql.schema.GraphQLSchema;
import graphql.schema.idl.RuntimeWiring;
import graphql.schema.idl.SchemaGenerator;
import graphql.schema.idl.SchemaParser;
import graphql.schema.idl.TypeDefinitionRegistry;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.net.URL;

import static graphql.schema.idl.TypeRuntimeWiring.newTypeWiring;

@Component
@Primary
public class GraphQLProvider {

    @Autowired
    GraphQLDataFetchers graphQLDataFetchers;

    @Bean
    GraphQLSchema schema() {
        URL url = Resources.getResource("schema.graphqls");
        URL personUrl = Resources.getResource("person.graphql");
        URL clientUrl = Resources.getResource("client.graphql");
        URL locationUrl = Resources.getResource("location.graphql");
        URL missionUrl = Resources.getResource("mission.graphql");
        URL skillUrl = Resources.getResource("skill.graphql");
        String sdl = null;
        try {
            sdl = Resources.toString(url, Charsets.UTF_8)
                    .concat(Resources.toString(personUrl, Charsets.UTF_8))
                    .concat(Resources.toString(clientUrl, Charsets.UTF_8))
                    .concat(Resources.toString(locationUrl, Charsets.UTF_8))
                    .concat(Resources.toString(missionUrl, Charsets.UTF_8))
                    .concat(Resources.toString(skillUrl, Charsets.UTF_8));
        } catch (IOException e) {
            e.printStackTrace();
        }
        TypeDefinitionRegistry typeRegistry = new SchemaParser().parse(sdl);
        RuntimeWiring runtimeWiring = buildWiring();
        SchemaGenerator schemaGenerator = new SchemaGenerator();
        return schemaGenerator.makeExecutableSchema(typeRegistry, runtimeWiring);
    }

    private RuntimeWiring buildWiring() {
        return RuntimeWiring.newRuntimeWiring()
                .scalar(DateScalarConfiguration.DATE)
                .scalar(UploadScalarConfiguration.UPLOAD)
                .directive("auth", new AuthorizationDirective())
                .directive("authOrSelf", new AuthorizationOrSelfDirective())
                .type(newTypeWiring("QueryType")
                        .dataFetcher("login", graphQLDataFetchers.authentification.login())
                        .dataFetcher("persons", graphQLDataFetchers.person.getPersons())
                        .dataFetcher("person", graphQLDataFetchers.person.getPerson())
                        .dataFetcher("personByName", graphQLDataFetchers.person.getPersonByName())
                        .dataFetcher("missions", graphQLDataFetchers.mission.getMissions())
                        .dataFetcher("mission", graphQLDataFetchers.mission.getMission())
                        .dataFetcher("clients", graphQLDataFetchers.client.getClients())
                        .dataFetcher("locations", graphQLDataFetchers.location.getLocations())
                        .dataFetcher("skills", graphQLDataFetchers.skill.getSkills())
                        .dataFetcher("missionSkills", graphQLDataFetchers.skill.getMissionSkillsByMission())
                        .dataFetcher("collaborators", graphQLDataFetchers.person.getCollaborators())
                        .dataFetcher("personSkills", graphQLDataFetchers.skill.getPersonSkills())
                        .dataFetcher("candidatesSuggestions", graphQLDataFetchers.candidate.getCandidates())
                )
                .type(newTypeWiring("MutationType")
                        .dataFetcher("createPerson", graphQLDataFetchers.person.createPerson())
                        .dataFetcher("editPerson", graphQLDataFetchers.person.editPerson())
                        .dataFetcher("deletePerson", graphQLDataFetchers.person.deletePerson())
                        .dataFetcher("createMission", graphQLDataFetchers.mission.createMission())
                        .dataFetcher("editMission", graphQLDataFetchers.mission.editMission())
                        .dataFetcher("deleteMission", graphQLDataFetchers.mission.deleteMission())
                        .dataFetcher("createLocation", graphQLDataFetchers.location.createLocation())
                        .dataFetcher("addAuth", graphQLDataFetchers.authentification.addAuth())
                        .dataFetcher("createClient", graphQLDataFetchers.client.createClient())
                        .dataFetcher("addMissionCandidate", graphQLDataFetchers.application.addMissionCandidate())
                        .dataFetcher("removeMissionCandidate", graphQLDataFetchers.application.removeMissionCandidate())
                        .dataFetcher("createSkill", graphQLDataFetchers.skill.createSkill())
                        .dataFetcher("createMissionSkill", graphQLDataFetchers.skill.createMissionSkill())
                        .dataFetcher("deleteMissionSkills", graphQLDataFetchers.skill.deleteMissionSkills())
                        .dataFetcher("createPersonSkill", graphQLDataFetchers.skill.createPersonSkill())
                        .dataFetcher("deletePersonSkill", graphQLDataFetchers.skill.deletePersonSkills())
                        .dataFetcher("updateSkills", graphQLDataFetchers.skill.updateSkills())
                        .dataFetcher("createSkills", graphQLDataFetchers.skill.createSkills())
                        .dataFetcher("deleteSkills", graphQLDataFetchers.skill.deleteSkills())
                        .dataFetcher("deleteClients", graphQLDataFetchers.client.deleteClients())
                        .dataFetcher("deleteLocations", graphQLDataFetchers.location.deleteLocations())
                        .dataFetcher("updateClients", graphQLDataFetchers.client.updateClients())
                        .dataFetcher("updateLocations", graphQLDataFetchers.location.updateLocations())
                        .dataFetcher("createLocations", graphQLDataFetchers.location.createLocations())
                        .dataFetcher("createClientsWithLocations", graphQLDataFetchers.client.createClientsWithLocations())
                        .dataFetcher("updateClientWithLocation", graphQLDataFetchers.client.updateClientWithLocation())
                        .dataFetcher("editProfils", graphQLDataFetchers.person.editProfils())
                        .dataFetcher("editAuth", graphQLDataFetchers.authentification.editAuth())
                        .dataFetcher("duplicateMission", graphQLDataFetchers.mission.duplicateMission())
                )
                .type(newTypeWiring("Mission")
                        .dataFetcher("client", graphQLDataFetchers.client.getClientFromLocation())
                        .dataFetcher("client_location", graphQLDataFetchers.location.getLocation())
                        .dataFetcher("created_by", graphQLDataFetchers.person.getPerson())
                        .dataFetcher("candidates", graphQLDataFetchers.person.getMissionCandidatesFrom())
                        .dataFetcher("skills", graphQLDataFetchers.skill.getMissionSkillsByMission())
                        .dataFetcher("replaced", graphQLDataFetchers.person.getPerson())
                )
                .type(newTypeWiring("Location")
                        .dataFetcher("client", graphQLDataFetchers.client.getClient())
                        .dataFetcher("missions", graphQLDataFetchers.mission.getMissionsByClientLocation())
                )
                .type(newTypeWiring("Person")
                        .dataFetcher("client", graphQLDataFetchers.client.getClientFromLocation())
                        .dataFetcher("manager", graphQLDataFetchers.person.getManager())
                        .dataFetcher("client_location", graphQLDataFetchers.location.getLocation())
                        .dataFetcher("skills", graphQLDataFetchers.skill.getPersonSkills())
                )
                .type(newTypeWiring("MissionSkill")
                        .dataFetcher("mission", graphQLDataFetchers.mission.getMission())
                        .dataFetcher("skill", graphQLDataFetchers.skill.getSkill())
                )
                .type(newTypeWiring("Skill")
                        .dataFetcher("missionSkills", graphQLDataFetchers.skill.getMissionSkillsBySkill())
                )
                .type(newTypeWiring("PersonSkill")
                        .dataFetcher("person", graphQLDataFetchers.person.getPerson())
                        .dataFetcher("skill", graphQLDataFetchers.skill.getSkill())
                )
                .build();
    }

    /*
    @Override
    public CompletableFuture<ExecutionResult> invoke(GraphQLInvocationData invocationData, WebRequest webRequest) {
        ExecutionInput executionInput = ExecutionInput.newExecutionInput()
                .query(invocationData.getQuery())
                .context(webRequest)
                .operationName(invocationData.getOperationName())
                .variables(invocationData.getVariables())
                .build();
        return graphQL.executeAsync(executionInput);
    }
     */
}
