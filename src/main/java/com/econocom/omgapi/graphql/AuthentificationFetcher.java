package com.econocom.omgapi.graphql;

import com.econocom.omgapi.databaseconnectivity.OmgDataSource;
import com.econocom.omgapi.databaseconnectivity.QueryBuilder;
import graphql.schema.DataFetcher;

import java.util.ArrayList;

public class AuthentificationFetcher {

    OmgDataSource dataSource;

    public AuthentificationFetcher(OmgDataSource dataSource) {
        this.dataSource = dataSource;
    }

    public DataFetcher login() {
        return dataFetchingEnvironment -> {
            String email = dataFetchingEnvironment.getArgument("email");
            String password = dataFetchingEnvironment.getArgument("password");
            return dataSource.login(new Object[]{email, password});
        };
    }

    public DataFetcher addAuth(){
        return dataFetchingEnvironment -> {
            String email = dataFetchingEnvironment.getArgument("email");
            String password = dataFetchingEnvironment.getArgument("password");
            Integer person_id = dataFetchingEnvironment.getArgument("person_id");
            String query = new QueryBuilder()
                    .buildSimpleCreate(new String[]{"person_id", "email", "password"}, "authentification")
                    .getSql();
            return dataSource.simpleCreate(query,new Object[]{person_id,email,password});
        };
    }

    public DataFetcher editAuth() {
        return dataFetchingEnvironment -> {
            String email = dataFetchingEnvironment.getArgument("email");
            String password = dataFetchingEnvironment.getArgument("password");
            Integer person_id = dataFetchingEnvironment.getArgument("person_id");

            ArrayList<String> fields = new ArrayList<>();
            fields.add("email");

            if (password != null)
                fields.add("password");

            String sqlQuery = new QueryBuilder()
                    .buildSimpleUpdate(fields.toArray(new String[0]),
                            "authentification",
                            "person_id")
                    .getSql();
            ArrayList<Object> params = new ArrayList<>();
            params.add(email);
            if (password != null)
                params.add(password);
            params.add(person_id);

            return dataSource.simpleUpdate(sqlQuery, params.toArray());
        };
    }
}
