package com.econocom.omgapi.databaseconnectivity;

import com.econocom.omgapi.token.Token;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.jdbc.repository.config.AbstractJdbcConfiguration;
import org.springframework.data.jdbc.repository.config.EnableJdbcRepositories;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

@Configuration
@EnableJdbcRepositories
public class OmgDataSource extends AbstractJdbcConfiguration {

    private final JdbcTemplate jdbcTemplate;

    @Autowired
    public OmgDataSource(JdbcTemplate jdbcTemplate) {
        this.jdbcTemplate = jdbcTemplate;
    }

    /**
     * Query a list of row
     *
     * @param sql        query string.
     * @param parameters array of parameters given to the query string.
     * @return list of row, represented as a map of object.
     */
    public List<Map<String, Object>> simpleQuery(String sql, Object[] parameters) {
        return this.jdbcTemplate.queryForList(sql, parameters);
    }

    /**
     * Insert row
     *
     * @param sql    insert string.
     * @param values array of values to insert.
     * @return the identifier of the created row.
     */
    public Integer simpleCreate(String sql, Object[] values) {
        KeyHolder keyholder = new GeneratedKeyHolder();
        this.jdbcTemplate.update(
                connection -> {
                    int i = 1;
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    for (Object value : values) {
                        ps.setObject(i++, value);
                    }
                    return ps;
                },
                keyholder);
        return Objects.requireNonNull(keyholder.getKey()).intValue();
    }

    /**
     * Update row
     *
     * @param sql    update string.
     * @param values array of values to update.
     * @return number of row updated.
     */
    public Integer simpleUpdate(String sql, Object[] values) {
        return this.jdbcTemplate.update(sql, values);
    }

    /**
     * Delete row
     *
     * @param sql    delete string.
     * @param values array of conditional values.
     * @return true if a row has been deleted.
     */
    public boolean simpleDelete(String sql, Object[] values) {
        return this.jdbcTemplate.update(sql, values) == 1;
    }

    /**
     * Query the fieldnames of a specific table.
     *
     * @param tableName name of the targeted table.
     * @return a list of field names.
     */
    public List<String> queryFields(String tableName) {
        String sqlQuery = "SELECT column_name FROM INFORMATION_SCHEMA.COLUMNS WHERE TABLE_NAME = ?";
        return this.jdbcTemplate.queryForList(sqlQuery, tableName)
                .stream()
                .map(map -> (String) map.get("column_name"))
                .collect(Collectors.toList());
    }

    /**
     * Check credentials and return a corresponding token.
     *
     * @param parameters array of parameters to check credentials.
     * @return a token string.
     */
    public String login(Object[] parameters) {
        String sql = "SELECT p.id, p.profil FROM authentification AS a " +
                "INNER JOIN person AS p ON a.person_id = p.id " +
                "WHERE a.email=? AND a.password=?";
        try {
            Token token = this.jdbcTemplate.queryForObject(
                    sql,
                    parameters,
                    this::loginMapRow);

            assert token != null;
            return token.generate();
        } catch (EmptyResultDataAccessException e) {
            throw new RuntimeException(e);
        }
        catch (Exception e) {
            // TODO: create own exception
            throw e;
        }
    }

    private Token loginMapRow(ResultSet rs, int rowNum) throws SQLException {
        return new Token(Integer.toString(rs.getInt("id")), rs.getString("profil"));
    }

    /**
     * Insert a person
     *
     * @param parameters array of value to insert.
     * @return the id of the inserted person.
     * @deprecated
     */
    public Integer addPerson(Object[] parameters) {
        String sql = "INSERT INTO person (lastname,firstname,profil) VALUES (?, ?, ?::person_profil)";
        KeyHolder keyholder = new GeneratedKeyHolder();
        this.jdbcTemplate.update(
                connection -> {
                    PreparedStatement ps = connection.prepareStatement(sql, new String[]{"id"});
                    for (int i = 0; i < parameters.length; i++) {
                        ps.setObject(i + 1, parameters[i]);
                    }
                    return ps;
                },
                keyholder);

        return Objects.requireNonNull(keyholder.getKey()).intValue();

    }

    /**
     * Insert a authentification
     *
     * @param paramaters array of value to insert.
     * @deprecated
     */
    public void setRegister(Object[] paramaters) {
        String sql = "INSERT INTO authentification (person_id,email,password) VALUES (?, ?, ?)";
        this.jdbcTemplate.update(
                sql,
                paramaters);
    }

    /**
     * Query enumeration list
     *
     * @return Array of enumeration values.
     * @deprecated
     */
    public String[] getEnum() {

        List<String> resultat = new ArrayList<>();
        String sql = "SELECT * from enum_range(NULL::person_profil)";
        List<Map<String, Object>> res = this.jdbcTemplate.queryForList(
                sql);

        for (Map<String, Object> maps : res) {
            for (Map.Entry<String, Object> entry : maps.entrySet()) {
                resultat.add(entry.getValue().toString());
            }
        }
        String result = resultat.get(0);
        return (result.substring(1, result.length() - 1).split(","));
    }

}
