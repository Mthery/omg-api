package com.econocom.omgapi.databaseconnectivity;

import graphql.schema.DataFetchingFieldSelectionSet;

import java.util.Map;

public class QueryBuilder {

    private String sql;
    private boolean hasQueryCondition;

    public QueryBuilder() {
        this.sql = "";
        this.hasQueryCondition = false;
    }

    /**
     * Get the generated SQL string
     *
     * @return SQL string query.
     */
    public String getSql() {
        return this.sql;
    }

    /**
     * Create a SQL string query
     *
     * @param selectFields    array of fieldnames whose fields are required in return.
     * @param tableName       name of the targeted table.
     * @param conditionFields array of fieldnames use to filter result.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleQuery(String[] selectFields, String tableName, String[] conditionFields) {
        String sql = "SELECT ";

        // Add specific queried fields
        if (selectFields == null || selectFields.length == 0) {
            sql += "* ";
        } else {
            sql += String.join(" ,", selectFields);
        }

        sql += " FROM " + tableName;

        // Add filter
        if (conditionFields != null && conditionFields.length != 0) {
            sql += " WHERE " + String.join(" = ? AND ", conditionFields);
            sql += " = ?";
            this.hasQueryCondition = true;
        }
        this.sql += sql;
        return this;
    }

    public QueryBuilder buildNaturalJoin(String[] selectFields, String tableName, String[] jointedTablesNames, String[] conditionFields) {
        StringBuilder sql = new StringBuilder("SELECT ");

        // Add specific queried fields
        if (selectFields == null || selectFields.length == 0) {
            sql.append("* ");
        } else {
            sql.append(String.join(" ,", selectFields));
        }

        sql.append(" FROM ").append(tableName);

        // Add join
        for (String jointedTablesName : jointedTablesNames) {
            sql.append(" NATURAL FULL JOIN ").append(jointedTablesName);
        }

        // Add filter
        if (conditionFields != null && conditionFields.length != 0) {
            sql.append(" WHERE ").append(String.join(" = ? AND ", conditionFields));
            sql.append(" = ?");
            this.hasQueryCondition = true;
        }
        this.sql += sql.toString();
        return this;
    }

    /**
     * Create a SQL string query with jointure
     *
     * @param selectFields       array of fieldnames whose fields are required in return.
     * @param tableName          name of the targeted table.
     * @param jointedTablesNames name of the jointed tables.
     * @param joinConditions     join sql conditions.
     * @param conditionFields    array of fieldnames use to filter result.
     * @param joinType           type of join to be perfomed.
     * @return QueryBuilder
     */
    public QueryBuilder buildJoinQuery(String[] selectFields, String tableName, String[] jointedTablesNames,
                                       String[] joinConditions, String[] conditionFields, String joinType) {
        StringBuilder sql = new StringBuilder("SELECT ");

        // Add specific queried fields
        if (selectFields == null || selectFields.length == 0) {
            sql.append("* ");
        } else {
            sql.append(String.join(" ,", selectFields));
        }

        sql.append(" FROM ").append(tableName);

        // Add join
        for (int i = 0; i < jointedTablesNames.length; i++) {
            sql.append(" " + joinType + " JOIN ").append(jointedTablesNames[i]).append(" ON ").append(joinConditions[i]);
        }

        // Add filter
        if (conditionFields != null && conditionFields.length != 0) {
            sql.append(" WHERE ").append(String.join(" = ? AND ", conditionFields));
            sql.append(" = ?");
            this.hasQueryCondition = true;
        }
        this.sql += sql.toString();
        return this;
    }

    /**
     * Create a SQL string query with INNER jointure
     *
     * @param selectFields       array of fieldnames whose fields are required in return.
     * @param tableName          name of the targeted table.
     * @param jointedTablesNames name of the jointed tables.
     * @param joinConditions     join sql conditions.
     * @param conditionFields    array of fieldnames use to filter result.
     * @return QueryBuilder
     */
    public QueryBuilder buildJoinQuery(String[] selectFields, String tableName, String[] jointedTablesNames,
                                       String[] joinConditions, String[] conditionFields) {
        return this.buildJoinQuery(selectFields, tableName, jointedTablesNames, joinConditions,
                conditionFields, "INNER");
    }

    /**
     * Create a SQL string query with a IN condition
     * @param selectFields Array of fieldnames whose fields are required in return.
     * @param tableName Name of the targeted table.
     * @param conditionFields Fieldname used to filter result.
     * @param intervals Array of values for the interval.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleIntervalQuery(String[] selectFields, String tableName,
                                                 String conditionFields, String[] intervals) {
        StringBuilder sql = new StringBuilder("SELECT ");

        // Add specific queried fields
        if (selectFields == null || selectFields.length == 0) {
            sql.append("* ");
        } else {
            sql.append(String.join(" ,", selectFields));
        }

        sql.append(" FROM ").append(tableName);

        sql.append(" WHERE ").append(conditionFields).append(" IN (");
        sql.append(String.join(", ", intervals));
        sql.append(")");

        this.sql += sql.toString();

        return this;
    }

    /**
     * Create a SQL string update
     *
     * @param updatedFields   array of fieldnames whose fields need to be updated.
     * @param tableName       name of the targeted table.
     * @param conditionFields array of fieldnames use to filter targeted row, id is already use as conditional field.
     * @return QueryBuilder
     * @deprecated
     */
    public QueryBuilder buildSimpleUpdate(String[] updatedFields, String tableName, String[] conditionFields) {
        String sql = "UPDATE " + tableName + " SET ";

        // Add fields that need to be updated
        sql += String.join(" = ?, ", updatedFields);
        sql += "= ?";

        // Add filter
        if (conditionFields != null && conditionFields.length != 0) {
            sql += " WHERE " + String.join(" = ? AND ", conditionFields);
            sql += " = ?";
        }

        sql += "WHERE id=?";
        this.sql += sql;
        return this;
    }

    /**
     * Create a SQL string update
     *
     * @param updatedFields array of fieldnames whose fields need to be updated.
     * @param tableName     name of the targeted table.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleUpdate(String[] updatedFields, String tableName) {
        String sql = "UPDATE " + tableName + " SET ";

        // Add fields that need to be updated
        sql += String.join(" = ?, ", updatedFields);
        sql += "= ?";

        sql += " WHERE id=?";
        this.sql += sql;
        return this;
    }

    /**
     * Create a SQL string update
     *
     * @param updatedFields array of fieldnames whose fields need to be updated.
     * @param tableName     name of the targeted table.
     * @param enumFields    Map of key: fieldname, value: enumeration name.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleUpdate(String[] updatedFields, String tableName, Map<String, String> enumFields) {
        StringBuilder sql = new StringBuilder("UPDATE " + tableName + " SET ");

        // Add fields that need to be insert
        for (int i = 0; i < updatedFields.length; i++) {
            sql.append(updatedFields[i]).append("= ?");

            // If the field is of type enumeration
            if (enumFields.containsKey(updatedFields[i])) {
                sql.append("::").append(enumFields.get(updatedFields[i]));
            }

            if (i < updatedFields.length - 1) {
                sql.append(", ");
            }
        }

        sql.append(" WHERE id=?");
        this.sql += sql.toString();
        return this;
    }

    /**
     * Create a SQL string update with custom filter
     *
     * @param updatedFields   array of fieldnames whose fields need to be updated.
     * @param tableName       name of the targeted table.
     * @param conditionField  fieldname used to filter targeted row
     * @return QueryBuilder
     * @deprecated
     */
    public QueryBuilder buildSimpleUpdate(String[] updatedFields, String tableName, String conditionField) {
        String sql = "UPDATE " + tableName + " SET ";

        // Add fields that need to be updated
        sql += String.join(" = ?, ", updatedFields);
        sql += "= ?";

        // Add filter
        sql += " WHERE " + conditionField + " = ?";

        this.sql += sql;
        return this;
    }

    /**
     * Create a SQL string insert
     *
     * @param fields    array of fieldnames whose fields need to be inserted.
     * @param tableName name of the targeted table.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleCreate(String[] fields, String tableName) {
        this.sql += "INSERT INTO " + tableName + "(" + String.join(", ", fields) +
                ") VALUES ( ?" + ", ?".repeat(fields.length - 1) + ")";
        return this;
    }

    /**
     * Create a SQL string insert with enumeration fields
     *
     * @param fields     array of fieldnames whose fields need to be inserted.
     * @param tableName  name of the targeted table.
     * @param enumFields Map of key: fieldname, value: enumeration name.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleCreate(String[] fields, String tableName, Map<String, String> enumFields) {
        StringBuilder sql = new StringBuilder("INSERT INTO ")
                .append(tableName)
                .append("(")
                .append(String.join(", ", fields))
                .append(") VALUES ( ");

        // Add fields that need to be insert
        for (int i = 0; i < fields.length; i++) {
            sql.append("?");

            // If the field is of type enumeration
            if (enumFields.containsKey(fields[i])) {
                sql.append("::").append(enumFields.get(fields[i]));
            }

            if (i < fields.length - 1) {
                sql.append(", ");
            }
        }
        sql.append(")");
        this.sql += sql.toString();
        return this;
    }

    /**
     * Create a SQL string delete with conditionnal fields
     *
     * @param tableName       name of the targeted table.
     * @param conditionFields array of fieldnames use to filter targeted row, id is already use as conditional field.
     * @return QueryBuilder
     */
    public QueryBuilder buildSimpleDelete(String tableName, String[] conditionFields) {
        this.sql += "DELETE FROM " + tableName + " WHERE " + String.join(" = ? AND ", conditionFields) + " = ?";
        return this;
    }

    /**
     * Add a query regex condition on existing sql string
     *
     * @param hasRegex boolean that enable the function.
     * @return QueryBuilder
     */
    public QueryBuilder addSearchRegex(boolean hasRegex) {
        if (hasRegex) {
            String sql = this.hasQueryCondition ? " AND " : " WHERE ";
            sql += "_search ILIKE ?";
            this.sql += sql;
        }
        return this;
    }
    /**
     * Add a query sort condition on existing sql string
     *
     * @param sortFilter boolean that enable the function.
     * @param field to be sorted.
     * @param direction of the sorting.
     * @param authorizedColumns Set of the fields selected.
     * @return QueryBuilder
     */
    public QueryBuilder addSortFilter(boolean sortFilter, String field, String direction, DataFetchingFieldSelectionSet authorizedColumns) throws Exception {
        if (sortFilter) {
            if (authorizedColumns.contains(field)) {
                String sql = " ORDER BY " + " " + field + " " + direction;
                this.sql += sql;
            }
            else throw new Exception();
        }
        return this;
    }

    public QueryBuilder addOrCondition(String[] conditionFields, String[] overrideValues, String[] conditions) {
        StringBuilder sql = new StringBuilder();
        sql.append(this.hasQueryCondition ? " AND (" : " WHERE (");
        for (int i=0; i<conditionFields.length; i++) {
            sql.append(conditionFields[i])
                    .append(" ")
                    .append(conditions[i])
                    .append(" ")
                    .append(overrideValues[i]);
            if (i+1 < conditionFields.length) {
                sql.append(" OR ");
            }
        }
        this.hasQueryCondition = true;
        sql.append(")");
        this.sql += sql.toString();
        return this;
    }

    public QueryBuilder addSpecificCondition(String conditionFields, String condition) {
        StringBuilder sql = new StringBuilder();
        sql.append(this.hasQueryCondition ? " AND " : " WHERE ");
        sql.append(conditionFields + " " + condition + " ?");
        this.sql += sql.toString();
        this.hasQueryCondition = true;
        return this;
    }

}
