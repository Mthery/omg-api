package com.econocom.omgapi.datafeeder;

import com.econocom.omgapi.mock.MockContext;
import com.econocom.omgapi.token.Token;
import com.github.javafaker.Faker;
import com.github.javafaker.service.FakeValuesService;
import com.github.javafaker.service.RandomService;
import graphql.ExecutionInput;
import graphql.GraphQL;
import graphql.kickstart.servlet.context.GraphQLServletContext;
import graphql.schema.GraphQLSchema;
import org.flywaydb.core.Flyway;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.*;

@Component
public class OmgDataFeeder {

    @Autowired
    ApplicationArguments appArgs;

    @Autowired
    private Environment env;

    @Autowired
    private GraphQLSchema schema;

    @PostConstruct
    public void init() {
          Flyway flyway = Flyway.configure().dataSource(env.getProperty("spring.datasource.url"),
                        env.getProperty("spring.datasource.username"),
                        env.getProperty("spring.datasource.password"))
                        .load();
                flyway.clean();
                flyway.migrate();

        //Generate Fake data
        if (appArgs.getOptionValues("DataGenParam") != null && appArgs.getOptionValues("DataGenParam").get(0).equals("1")) {
            GraphQL graphQL = GraphQL.newGraphQL(schema).build();

            Faker faker = new Faker(new Locale("fr"));
            FakeValuesService fakeValuesService = new FakeValuesService(
                    new Locale("fr"), new RandomService());
            Integer NUM_ITERATIONS = 25;

            String queryCreatePerson = "mutation createPerson($createPerson: createPerson!) {  createPerson(createPerson: $createPerson) }";
            String queryCreateMission = "mutation createMission($createMission: createMission!) {  createMission(createMission: $createMission) }";
            String queryCreateClient = "mutation createClient($createClient: createClient!) {  createClient(createClient: $createClient) }";
            String queryCreateLocation = "mutation createLocation($createLocation: createLocation!) {  createLocation(createLocation: $createLocation) }";
            String queryCreateAuth = "mutation addAuth($person_id: Int!, $email: String!, $password: String!) { addAuth(person_id: $person_id, email: $email, password: $password) }";
            String queryCreateSkill = "mutation createSkill($label: String!) { createSkill(label: $label) }";
            String queryCreateMissionSkill = "mutation createMissionSkill($createMissionSkill: [createMissionSkill!]) { createMissionSkill(createMissionSkill: $createMissionSkill) }";
            String queryCreatePersonSkill = "mutation createPersonSkill($createPersonSkill: [createPersonSkill!]) { createPersonSkill(createPersonSkill: $createPersonSkill) }";

            // Person & Auth
            Map<String, Object> manager = new HashMap<>();
            manager.put("lastname", faker.name().lastName());
            manager.put("firstname", faker.name().firstName());
            manager.put("profil", fakeValuesService.regexify("MANAGER"));
            manager.put("email", fakeValuesService.letterify("?????@econocom.com"));
            manager.put("phone", faker.numerify("06#######"));

            Map<String, Object> variablesCreateManager = Map.ofEntries(
                    Map.entry("createPerson", manager)
            );

            graphQL.execute(ExecutionInput.newExecutionInput().variables(variablesCreateManager).query(queryCreatePerson).build());

            for (int i = 0; i < 100; i++) {
                Map<String, Object> person = new HashMap<>();
                person.put("lastname", faker.name().lastName());
                person.put("firstname", faker.name().firstName());
                person.put("profil", fakeValuesService.regexify("CANDIDATE|FREELANCE|CONTRACTOR|INTERN|MANAGER|IC|HR"));
                person.put("email", fakeValuesService.letterify("?????@econocom.com"));
                person.put("phone", faker.numerify("06#######"));

                Map<String, Object> subFields = new HashMap<>();

                switch ((String) person.get("profil")) {
                    case "INTERN":
                    case "CANDIDATE":
                    case "FREELANCE":
                    case "CONTRACTOR":
                        subFields.put("manager_id", 1);
                        person.put("collaborator", subFields);
                }

                Map<String, Object> variablesCreatePerson = Map.ofEntries(
                        Map.entry("createPerson", person)
                );

                graphQL.execute(ExecutionInput.newExecutionInput().variables(variablesCreatePerson).query(queryCreatePerson).build());
            }

            // Création du compte superadmin, seulement pour le dev
            Map<String, Object> person = new HashMap<>();
            person.put("lastname", "Admin");
            person.put("firstname", "Admin");
            person.put("profil", "IC");
            person.put("email", "admin@econocom.com");
            person.put("phone", 06);

            Map<String, Object> variablesCreatePerson = Map.ofEntries(
                    Map.entry("createPerson", person)
            );

            graphQL.execute(ExecutionInput.newExecutionInput().variables(variablesCreatePerson).query(queryCreatePerson).build());

            Map<String, Object> variablesCreateAuth = Map.ofEntries(
                    Map.entry("person_id", 194), // 194 car en comptant les vrais données, l'admin est en 194
                    Map.entry("email", "da96bed0e5a65870f41bf70eeeafe5e4415bc2d0320fcbd8677f4579d3333c54"), // admin@econocom.com
                    Map.entry("password", "8c6976e5b5410415bde908bd4dee15dfb167a9c873fc4bb8a81f6f2ab448a918") // admin
            );
            graphQL.execute(ExecutionInput.newExecutionInput().variables(variablesCreateAuth).query(queryCreateAuth).build());

            // Client & Missions
            for (int i = 0; i < NUM_ITERATIONS; i++) {
                Map<String, Object> variableCreateClient = Map.ofEntries(
                        Map.entry("createClient", Map.ofEntries(
                                Map.entry("label", faker.company().name())
                        ))
                );
                int client_id = (int) ((Map<String, Object>) graphQL.execute(ExecutionInput
                        .newExecutionInput()
                        .variables(variableCreateClient)
                        .query(queryCreateClient)
                        .build())
                        .getData())
                        .get("createClient");

                Map<String, Object> variablesCreateLocation = Map.ofEntries(
                        Map.entry("createLocation", Map.ofEntries(
                                Map.entry("label", faker.address().cityName()),
                                Map.entry("client_id", client_id)
                        ))
                );
                int loc_id = (int) ((Map<String, Object>) graphQL.execute(ExecutionInput
                        .newExecutionInput()
                        .variables(variablesCreateLocation)
                        .query(queryCreateLocation)
                        .build())
                        .getData())
                        .get("createLocation");

                Map<String, Object> createMission = new HashMap<>();
                createMission.put("client_location_id", loc_id);
                createMission.put("type", fakeValuesService.regexify("INFO|TA"));
                createMission.put("priority", fakeValuesService.regexify("P1|P2|P3"));
                createMission.put("status", fakeValuesService.regexify("REPLACEMENT|NEW-WON|CRITICAL|OPEN|CLOSED|WIN|LOSS|UNANSWERED|RECRUITING-GROUND|SENT|NEED|SWITCH"));
                createMission.put("description", faker.lorem().paragraph());
                createMission.put("date_out", new Random().nextBoolean() ?
                        faker.date().between(
                                Date.from(LocalDate.now().plusMonths(2).atStartOfDay(ZoneId.systemDefault()).toInstant()),
                                Date.from(LocalDate.now().plusMonths(6).atStartOfDay(ZoneId.systemDefault()).toInstant())
                        ).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString() : null);
                createMission.put("date_start", faker.date().between(
                        Date.from(LocalDate.now().atStartOfDay(ZoneId.systemDefault()).toInstant()),
                        Date.from(LocalDate.now().plusMonths(2).atStartOfDay(ZoneId.systemDefault()).toInstant())
                ).toInstant().atZone(ZoneId.systemDefault()).toLocalDate().toString());
                createMission.put("created_by", faker.number().numberBetween(1, 102));
                createMission.put("replaced", faker.number().numberBetween(1, 102));
                createMission.put("agency", faker.number().numberBetween(521, 523));

                Map<String, Object> variablesCreateMission = Map.ofEntries(
                        Map.entry("createMission", createMission)
                );

                graphQL.execute(ExecutionInput.newExecutionInput()
                        .variables(variablesCreateMission)
                        .context(new MockContext(new Token("102", "IC").generate()))
                        .query(queryCreateMission).build());
            }

            // Skills
            for (int i = 1; i < 6; i++) {
                Map<String, Object> variablesCreateSkill = Map.ofEntries(
                    Map.entry("label", "Angular " + i)
                );
                graphQL.execute(ExecutionInput.newExecutionInput().variables(variablesCreateSkill).query(queryCreateSkill).build());
            }

            // Mission Skills
            for (int i = 0; i < NUM_ITERATIONS; i++) {
                Map<String, Object> createMissionSkill = new HashMap<>();
                createMissionSkill.put("mission_id", i+1);
                createMissionSkill.put("skill_id", faker.number().numberBetween(1, 5));
                createMissionSkill.put("level", faker.regexify("Débutant \\(0 à 2 ans\\)|Intermédiaire \\(2 à 4 ans\\)|Confirmé \\(4 à 8 ans\\)|Sénior \\(plus de 8 ans\\)"));
                createMissionSkill.put("mandatory", faker.random().nextBoolean());

                ArrayList<Map<String, Object>> arrayCreateMissionSkill = new ArrayList<>();
                arrayCreateMissionSkill.add(createMissionSkill);

                Map<String, Object> variable = new HashMap<>();
                variable.put("createMissionSkill", arrayCreateMissionSkill);

                graphQL.execute(ExecutionInput.newExecutionInput().variables(variable).query(queryCreateMissionSkill).build());
            }

            // Person Skills
            for (int i = 0; i < 100; i++) {
                Map<String, Object> createPersonSkill = new HashMap<>();
                createPersonSkill.put("person_id", i+1);
                createPersonSkill.put("skill_id", faker.number().numberBetween(1, 5));
                createPersonSkill.put("level", faker.regexify("Débutant \\(0 à 2 ans\\)|Intermédiaire \\(2 à 4 ans\\)|Confirmé \\(4 à 8 ans\\)|Sénior \\(plus de 8 ans\\)"));

                ArrayList<Map<String, Object>> arrayCreatePersonSkill = new ArrayList<>();
                arrayCreatePersonSkill.add(createPersonSkill);

                Map<String, Object> variable = new HashMap<>();
                variable.put("createPersonSkill", arrayCreatePersonSkill);

                graphQL.execute(ExecutionInput.newExecutionInput().variables(variable).query(queryCreatePersonSkill).build());
            }
        }
    }

}
