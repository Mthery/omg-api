package com.econocom.omgapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OmgApiApplication {

    public static void main(String[] args) {
        SpringApplication.run(OmgApiApplication.class, args);
    }

}
