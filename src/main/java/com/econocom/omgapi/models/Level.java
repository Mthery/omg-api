package com.econocom.omgapi.models;

public enum Level {
    DEBUTANT("Débutant (0 à 2 ans)", 1),
    INTERMEDIAIRE("Intermédiaire (2 à 4 ans)", 2),
    CONFIRME("Confirmé (4 à 8 ans)", 3),
    SENIOR("Sénior (plus de 8 ans)", 4);

    private final String value;
    private final Integer key;

    Level(String value, Integer key) {
        this.value = value;
        this.key = key;
    }

    public String getValue() {
        return this.value;
    }

    public Integer getKey() {
        return this.key;
    }
}
