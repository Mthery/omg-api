package com.econocom.omgapi.models;

import java.util.ArrayList;
import java.util.List;

public class Collaborator {
    private Integer id;
    private Integer percentage;
    private List<Skill> skills = new ArrayList<>();

    public Collaborator(Integer id, List<Skill> skills) {
        this.id = id;
        this.skills = skills;
    }

    public Collaborator(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return this.id;
    }

    public Integer getPercentage() {
        return this.percentage;
    }

    public List<Skill> getSkills() {
        return this.skills;
    }

    public void setPercentage(Integer percentage) {
        this.percentage = percentage;
    }

    public void addSkill(Skill skill) {
        this.skills.add(skill);
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Collaborator) {
            Collaborator c = (Collaborator) o;
            return c.getId().equals(this.id);
        }
        return false;
    }
}
