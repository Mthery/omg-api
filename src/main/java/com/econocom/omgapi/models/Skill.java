package com.econocom.omgapi.models;

public class Skill {
    private Integer id;
    private String level;

    public Skill(Integer id, String level) {
        this.id = id;
        this.level = level;
    }

    public Integer getId() {
        return this.id;
    }

    public String getLevel() {
        return this.level;
    }

    @Override
    public boolean equals(Object o) {
        if (o instanceof Skill) {
            Skill s = (Skill) o;
            return s.getId().equals(this.id);
        }
        return false;
    }
}
