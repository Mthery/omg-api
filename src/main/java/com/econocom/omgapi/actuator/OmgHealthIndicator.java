package com.econocom.omgapi.actuator;

import org.springframework.boot.actuate.health.Health;
import org.springframework.boot.actuate.health.HealthIndicator;
import org.springframework.stereotype.Component;

@Component
public class OmgHealthIndicator implements HealthIndicator {

    @Override
    public Health health() {
        /*
        TODO: Implements some health-checks
         */
        return Health.up().build();
    }
}
