package com.econocom.omgapi.token;

import io.jsonwebtoken.*;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

public class Token implements Serializable {

    // In Millisecond
    private static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60 * 1000;

    // TODO: Use secret from external file
    private final String secret = "OhMyGod";

    private final String userId;
    private final String userRole;

    public Token(String tokenString) throws Exception {
        try {
            Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(tokenString).getBody();
            this.userId = this.getClaimFromToken(Claims::getId, claims);
            this.userRole = claims.get("userRole").toString();
        } catch (SignatureException e) {
            throw new Exception("Invalid JWT signature.");
        } catch (MalformedJwtException e) {
            throw new Exception("Invalid JWT token.");
        } catch (ExpiredJwtException e) {
            throw new Exception("Expired JWT token.");
        } catch (UnsupportedJwtException e) {
            throw new Exception("Unsupported JWT token.");
        } catch (IllegalArgumentException e) {
            throw new Exception("JWT token compact of handler are invalid.");
        }
    }

    public Token(String userId, String userRole) {
        this.userId = userId;
        this.userRole = userRole;
    }

    /**
     * Generate a token string from this token parameter.
     *
     * @return a token string.
     */
    public String generate() {
        Map<String, Object> claims = new HashMap<>();
        return Jwts.builder()
                .setClaims(claims)
                .setId(this.userId)
                .claim("userRole", this.userRole)
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY))
                .signWith(SignatureAlgorithm.HS512, this.secret)
                .compact();
    }

    /**
     * Get the user id of the current token
     *
     * @return a user id.
     */
    public Integer getUserId() {
        return Integer.parseInt(this.userId);
    }

    /**
     * Get the user role of the current token
     *
     * @return a user role.
     */
    public String getUserRole() {
        return this.userRole;
    }

    private <T> T getClaimFromToken(Function<Claims, T> claimsResolver, Claims claims) {
        return claimsResolver.apply(claims);
    }

}
