//package com.econocom.omgapi.azure;
//
//import com.azure.storage.blob.*;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//
//@Configuration
//public class AzureStorageBlobClientConfig {
//
//    @Value("${azure.storage.connection-string}")
//    String connectionString;
//
//    @Value("${azure.storage.container.name}")
//    String containerName;
//
//    @Bean
//    public BlobClientBuilder getClient() {
//        BlobClientBuilder client = new BlobClientBuilder();
//        client.connectionString(connectionString);
//        client.containerName(containerName);
//        return client;
//    }
//}