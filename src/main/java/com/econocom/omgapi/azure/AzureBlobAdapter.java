//package com.econocom.omgapi.azure;
//
//import com.azure.storage.blob.BlobClient;
//import com.azure.storage.blob.BlobClientBuilder;
//import com.azure.storage.blob.models.BlobHttpHeaders;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//
//import javax.servlet.http.Part;
//import java.io.IOException;
//
//@Service
//public class AzureBlobAdapter {
//
//    @Autowired
//    BlobClientBuilder client;
//
//    public String upload(Part file, String name) {
//        if (file != null && file.getSize() > 0) {
//            try {
//                BlobClient blobClient = client.blobName(name).buildClient();
//                blobClient.upload(file.getInputStream(), file.getSize(), true);
//                BlobHttpHeaders headers = new BlobHttpHeaders();
//                headers.setContentType("application/pdf");
//                blobClient.setHttpHeaders(headers);
//                return blobClient.getBlobUrl();
//            } catch (IOException e) {
//                e.printStackTrace();
//            }
//        }
//        return null;
//    }
//
//
//}