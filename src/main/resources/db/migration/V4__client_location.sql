CREATE TABLE client_location
(
    id        INT GENERATED ALWAYS AS IDENTITY,
    client_id INT          NOT NULL,
    label     VARCHAR(255) NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk__client_location__client FOREIGN KEY (client_id) REFERENCES client (id) ON DELETE CASCADE,
    CONSTRAINT ck__not_empty CHECK ( NOT (label = ''))
);
