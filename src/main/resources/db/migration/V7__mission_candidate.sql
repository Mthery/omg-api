CREATE TABLE mission_candidate
(
    id              INT GENERATED ALWAYS AS IDENTITY,
    collaborator_id INT NOT NULL,
    mission_id INT NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk__mission_candidate__collaborator FOREIGN KEY (collaborator_id) REFERENCES collaborator (id) ON DELETE CASCADE,
    CONSTRAINT fk__mission_candidate__mission FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE,
    UNIQUE (collaborator_id, mission_id)
);

