CREATE TABLE authentification
(
    id        INT GENERATED ALWAYS AS IDENTITY,
    person_id INT          NOT NULL UNIQUE,
    email     VARCHAR(255) NOT NULL UNIQUE,
    password  VARCHAR(255) NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk__authentification__person FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
    CONSTRAINT ck__not_empty CHECK ( NOT (email = '' OR password = ''))
);