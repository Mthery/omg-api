CREATE OR REPLACE FUNCTION trigger_set_timestamp()
    RETURNS TRIGGER AS
$$
BEGIN
    NEW.date_edit = NOW();
    RETURN NEW;
END;
$$ LANGUAGE plpgsql;

CREATE TYPE person_profil AS ENUM ('CANDIDATE', 'CONTACT', 'FREELANCE', 'CONTRACTOR', 'INTERN', 'MANAGER', 'IC', 'HR');

CREATE TABLE person
(
    id        INT GENERATED ALWAYS AS IDENTITY,
    lastname  VARCHAR(255)  NOT NULL,
    firstname VARCHAR(255)  NOT NULL,
    email     VARCHAR(255),
    phone     VARCHAR(255),
    profil    person_profil NOT NULL,
    date_in   date NOT NULL DEFAULT NOW(),
    date_edit date          NOT NULL DEFAULT NOW(),

    PRIMARY KEY (id),

    CONSTRAINT ck__email CHECK ( email ~
                                 '^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@econocom.com$' ),
    CONSTRAINT ck__phone CHECK ( phone not like '%[^0-9+-.]%'),
    CONSTRAINT ck__not_empty CHECK ( NOT (lastname = '' OR firstname = ''))
);

CREATE TRIGGER set_timestamp
    BEFORE UPDATE
    ON person
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();
