CREATE TABLE skill
(
    id              INT GENERATED ALWAYS AS IDENTITY,
    label           VARCHAR(255) NOT NULL,

    CONSTRAINT ck__not_empty CHECK (NOT (label = '')),
    PRIMARY KEY (id)
);

CREATE TYPE level AS ENUM ('Débutant (0 à 2 ans)',
    'Intermédiaire (2 à 4 ans)',
    'Confirmé (4 à 8 ans)',
    'Sénior (plus de 8 ans)');

CREATE TABLE mission_skill
(
    id              INT GENERATED ALWAYS AS IDENTITY,
    mission_id      INT NOT NULL,
    skill_id        INT NOT NULL,
    level           level NOT NULL,
    mandatory       BOOLEAN NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk__mission_skill__mission_id FOREIGN KEY (mission_id) REFERENCES mission (id) ON DELETE CASCADE,
    CONSTRAINT fk__mission_skill__skill_id FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE,
    UNIQUE (mission_id, skill_id)
);

CREATE TABLE person_skill
(
    id INT GENERATED ALWAYS AS IDENTITY,
    person_id INT NOT NULL,
    skill_id INT NOT NULL,
    level level NOT NULL,

    PRIMARY KEY (id),
    CONSTRAINT fk__person_skill__person_id FOREIGN KEY (person_id) REFERENCES person (id) ON DELETE CASCADE,
    CONSTRAINT fk__person_skill__skill_id FOREIGN KEY (skill_id) REFERENCES skill (id) ON DELETE CASCADE,
    UNIQUE (person_id, skill_id)
);
