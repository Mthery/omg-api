CREATE TYPE mission_status AS ENUM('REPLACEMENT', 'NEW-WON', 'CRITICAL', 'OPEN', 'CLOSED', 'WIN', 'LOSS',
    'UNANSWERED', 'RECRUITING-GROUND', 'SENT', 'NEED', 'SWITCH');
CREATE TYPE mission_priority AS ENUM ('P3', 'P2', 'P1');
CREATE TYPE mission_type AS ENUM ('INFO', 'TA');
CREATE TYPE mission_agency AS ENUM ('521', '522', '523');

CREATE TABLE mission
(
    id                 INT GENERATED ALWAYS AS IDENTITY,
    client_location_id INT              NOT NULL,
    description        text,
    type               mission_type     NOT NULL,
    status             mission_status   NOT NULL,
    priority           mission_priority NOT NULL DEFAULT 'P3',
    adr                smallint,
    date_in            date             NOT NULL DEFAULT NOW(),
    date_start         date,
    date_edit          date             NOT NULL DEFAULT NOW(),
    date_out           date,
    created_by         INT              NOT NULL,
    replaced           INT,
    agency             mission_agency   NOT NULL,
    contact_phone      VARCHAR(255),
    contact_email      VARCHAR(255),

    CONSTRAINT fk__mission__client_location FOREIGN KEY (client_location_id) REFERENCES client_location (id),
    CONSTRAINT fk__mission__created_by FOREIGN KEY (created_by) REFERENCES person (id),
    CONSTRAINT fk__mission_person FOREIGN KEY (replaced) REFERENCES person (id),
    PRIMARY KEY (id),

    CONSTRAINT ck__date CHECK (date_out >= date_start AND date_edit >= date_in)
);

CREATE TRIGGER set_timestamp
    BEFORE UPDATE
    ON mission
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();