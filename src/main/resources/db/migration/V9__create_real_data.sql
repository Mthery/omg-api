-- Ajout de l'extension UNACCENT pour pouvoir obtenir des chaînes de caractère sans accents
CREATE EXTENSION IF NOT EXISTS unaccent;

-- Création d'une table pour y mettres les données du csv
CREATE TABLE tmp_mission
(
    date_in VARCHAR(255),
    created_by VARCHAR(255),
    client VARCHAR(255),
    poste VARCHAR(255),
    priority VARCHAR(2),
    status VARCHAR(50),
    date_start VARCHAR(255),
    experience VARCHAR(50),
    skill VARCHAR(255),
    skill_to_have VARCHAR(255),
    client_location VARCHAR(10),
    collab VARCHAR(255),
    candidate VARCHAR(255)
);

-- Remplissage de la table
COPY tmp_mission FROM 'C:\\temp\\Classeur.csv' DELIMITERS ',' CSV HEADER ENCODING 'LATIN2';


-- Création des clients et des client_locations grâce aux données du csv
INSERT INTO client (label)
SELECT DISTINCT LTRIM(RTRIM(INITCAP(client)))
FROM tmp_mission;

INSERT INTO client_location (client_id,
    label)
SELECT DISTINCT c.id,
    CASE WHEN m.client_location IS NULL THEN 'Lille'
        ELSE LTRIM(RTRIM(INITCAP(m.client_location)))
    END
FROM client c
    INNER JOIN tmp_mission m ON LTRIM(RTRIM(INITCAP(m.client))) = c.label;


-- Création des managers et ingénieurs commerciaux ayant créé les offres dans le csv
DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp (
    created_by VARCHAR(255)
);

INSERT INTO tmp (created_by)
SELECT DISTINCT LTRIM(RTRIM(INITCAP(created_by)))
FROM tmp_mission;

INSERT INTO person (lastname,
    firstname,
    profil)
SELECT created_by,
    CASE WHEN created_by = 'Doose' THEN 'Mickael'
        WHEN created_by = 'Wantiez' THEN 'Jonathan'
        WHEN created_by = 'Faugouin' THEN 'Céline'
        WHEN created_by = 'Blanchard' THEN 'Vincent'
        WHEN created_by = 'Deboudt' THEN 'Frédéric'
        WHEN created_by = 'Goeman' THEN 'Clémence'
        WHEN created_by = 'Leborgne' THEN 'Yann'
        WHEN created_by = 'Cousin' THEN 'Franck'
        WHEN created_by = 'Gebert' THEN 'Maxime'
        WHEN created_by = 'Julou' THEN 'Coralie'
     END,
     CASE WHEN created_by IN ('Wantiez', 'Doose', 'Blanchard', 'Deboudt', 'Cousin') THEN CAST('MANAGER' AS person_profil)
        ELSE CAST ('IC' AS person_profil)
    END
FROM tmp;

DROP TABLE tmp;

-- Création des missions
INSERT INTO mission (date_in,
     date_edit,
     client_location_id,
     description,
     type,
     status,
     priority,
     date_start,
     created_by,
     agency)
SELECT DISTINCT TO_DATE(m.date_in, 'DD.MM.YYYY'),
    TO_DATE(m.date_in, 'DD.MM.YYYY'),
    cl.id,
    m.poste,
    CAST('INFO' as mission_type),
    CASE WHEN m.status LIKE '%fermé' THEN CAST ('CLOSED' AS mission_status)
        WHEN m.status LIKE 'AO gagné' THEN CAST ('WIN' AS mission_status)
        WHEN m.status LIKE '%non répondu' THEN CAST ('UNANSWERED' AS mission_status)
        WHEN m.status LIKE '%ouvert' THEN CAST ('OPEN' AS mission_status)
        WHEN m.status LIKE '%perdu' THEN CAST ('LOSS' AS mission_status)
        WHEN m.status LIKE 'Besoin pourvu' THEN CAST ('SWITCH' AS mission_status)
        WHEN m.status LIKE 'Critique%' THEN CAST ('CRITICAL' AS mission_status)
        WHEN m.status LIKE '%projet gagné' THEN CAST ('NEW-WON' AS mission_status)
        WHEN m.status LIKE 'Remplacement%' THEN CAST ('REPLACEMENT' AS mission_status)
        WHEN m.status LIKE '%envoyée' THEN CAST ('SENT' AS mission_status)
        WHEN m.status LIKE '%de phase' THEN CAST ('RECRUITING-GROUND' AS mission_status)
    END,
    CAST(m.priority as mission_priority),
    TO_DATE(m.date_start, 'DD.MM.YYYY'),
    p.id,
    CAST('523' AS mission_agency)
FROM tmp_mission m
    INNER JOIN client c ON c.label = LTRIM(RTRIM(INITCAP(m.client)))
    INNER JOIN client_location cl ON cl.client_id = c.id
    INNER JOIN person p ON LTRIM(RTRIM(UPPER(p.lastname))) = LTRIM(RTRIM(UPPER(m.created_by)));


-- Création des skills
DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp (
    skill VARCHAR(255)
);

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 1)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 2)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 3)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 4)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 5)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 6)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 7)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 8)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 9)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 10)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill, '# ', 11)
FROM tmp_mission
WHERE skill IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill_to_have, '# ', 1)
FROM tmp_mission
WHERE skill_to_have IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill_to_have, '# ', 2)
FROM tmp_mission
WHERE skill_to_have IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill_to_have, '# ', 3)
FROM tmp_mission
WHERE skill_to_have IS NOT NULL;

INSERT INTO tmp (skill)
SELECT DISTINCT split_part(skill_to_have, '# ', 4)
FROM tmp_mission
WHERE skill_to_have IS NOT NULL;

INSERT INTO skill (label)
SELECT DISTINCT RTRIM(LTRIM(INITCAP(skill)))
FROM tmp
WHERE skill <> '';

DROP TABLE IF EXISTS tmp;


-- Création des collaborateurs et candidats
DROP TABLE IF EXISTS tmp;

CREATE TABLE tmp (
    collab VARCHAR(255)
);

INSERT INTO tmp (collab)
SELECT DISTINCT split_part(LTRIM(RTRIM(INITCAP(collab))), '/', 1) c
FROM tmp_mission
WHERE collab IS NOT NULL;

INSERT INTO tmp (collab)
SELECT DISTINCT split_part(LTRIM(RTRIM(INITCAP(collab))), '/', 2) c
FROM tmp_mission
WHERE collab IS NOT NULL;

INSERT INTO tmp (collab)
SELECT DISTINCT split_part(LTRIM(RTRIM(INITCAP(candidate))), '/', 1) c
FROM tmp_mission
WHERE candidate IS NOT NULL;

INSERT INTO tmp (collab)
SELECT DISTINCT split_part(LTRIM(RTRIM(INITCAP(candidate))), '/', 2) c
FROM tmp_mission
WHERE candidate IS NOT NULL;

INSERT INTO tmp (collab)
SELECT DISTINCT split_part(LTRIM(RTRIM(INITCAP(candidate))), '/', 1) c3
FROM tmp_mission
WHERE candidate IS NOT NULL;

INSERT INTO person (lastname,
    firstname,
    profil)
SELECT DISTINCT SPLIT_PART(LTRIM(RTRIM(INITCAP(collab))), ' ', 2),
    SPLIT_PART(LTRIM(RTRIM(INITCAP(collab))), ' ', 1),
    CAST('INTERN' AS person_profil)
FROM tmp
WHERE collab <> '';

DROP TABLE tmp;


-- Remplissage des mails
UPDATE person
SET email = CONCAT(LOWER(UNACCENT(firstname)), '.', LOWER(UNACCENT(lastname)), '@econocom.com');


-- Remplissage de la table Collaborator
INSERT INTO collaborator (id, manager_id)
SELECT id, (SELECT id FROM person WHERE profil = CAST('MANAGER' AS person_profil) LIMIT 1)
FROM person
WHERE profil = CAST('INTERN' AS person_profil);

-- Suppression de la table servant à stocker le csv
DROP TABLE tmp_mission;