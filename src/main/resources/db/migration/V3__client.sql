CREATE TABLE client
(
    id       INT GENERATED ALWAYS AS IDENTITY,
    label    VARCHAR(255) NOT NULL,
    date_in  date         NOT NULL DEFAULT NOW(),
    date_out date,

    PRIMARY KEY (id),

    CONSTRAINT ck__not_empty CHECK ( NOT (label = '')),
    CONSTRAINT ck__date CHECK (date_out >= date_in)
);
