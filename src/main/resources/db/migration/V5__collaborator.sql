CREATE TABLE collaborator
(
    id            INT     NOT NULL,
    manager_id    INT     NOT NULL,
    adr           smallint,
    is_requested  boolean NOT NULL DEFAULT FALSE,
    cv            text,
    date_edit     date    NOT NULL DEFAULT NOW(),

    PRIMARY KEY (id),
    CONSTRAINT fk__collaborator__person FOREIGN KEY (id) REFERENCES person (id) ON DELETE CASCADE,
    CONSTRAINT fk__collaborator__manager FOREIGN KEY (manager_id) REFERENCES person (id) ON DELETE RESTRICT

);

CREATE TRIGGER set_timestamp
    BEFORE UPDATE
    ON collaborator
    FOR EACH ROW
EXECUTE PROCEDURE trigger_set_timestamp();

