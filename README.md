# Outil de management et gestion

## Flyway

Les migrations [flyway](https://flywaydb.org/documentation/) permettent d'entretenir la BDD durant tout le développement.
Elles se trouvent dans le dossier ressources/db/migration

#### Dossier de configuration

```
flyway.url=
flyway.user=
flyway.password=
flyway.locations=filesystem:
```

#### Commande

```bash
gradle -Dflyway.configFiles=path/to/config/file.conf flywayClean flywayMigrate
```

### Application.properties

Les propriétés pour exécuter l'application sont stockées dans un fichier application.properties. Ici seront stockées les données pour se connecter à la base de donnés.

Le fichier application.properties doit se situer dans le dossier src/main/ressources (il faut donc le créer si vous ne l'avez pas, avec le contenu suivant)

### Création des données

Pour le script V20 (création des données réelles), il faut copier le fichier Classeur.csv sur votre poste, dans un dossier C:\temp

#### Dossier de configuration
```
spring.datasource.initialization-mode=always
spring.datasource.platform=postgres
spring.datasource.driver-class-name=org.postgresql.Driver
spring.datasource.url=jdbc:postgresql://path.to.db:port/db
spring.datasource.username=username
spring.datasource.password=password
management.endpoints.enabled-by-default=false
management.endpoint.health.enabled=true 
```

Dans cette configuration, il faut changer path.to.db:port par localhost:5432 (à moins que vous changiez le port par défaut de postgre)
Il faut aussi changer /db par /leNomDeLaBaseCrééeEnLocal
Et enfin il faut changer username et password par les credentials du propriétaire de la base (par défaut l'utilisateur est postgres, et le mdp est demandé au premier lancement de pgAdmin)

### Lancer l'application
Une fois le fichier de properties et la base de données mis en place, il suffit d'aller dans le fichier OmgApiApplication, et de cliquer sur la flêche verte à gauche de la fonction main.
Pour pouvoir accéder à l'application, il faut se trouver dans la table Authentification.
Pour se faire il y a 2 solutions, utiliser le compte de test, ou s'en créer un.
Pour le compte de test, il faut lancer l'application avec le paramètre DataGebParam = 1.
Pour se faire, cliquez (dans IntelliJ) sur le menu déroulant en haut à droite (à côté du bouton pour lancer l'aplication) et ajoutez dans Program arguments: --DataGebParam=1
Ensuite vous pourrez lancer l'application avec admin@econocom.com en tant qu'adresse mail, et admin en tant que password.
Pour créer une ligne dans la table Authentification, il faut d'abord créer une ligne dans la table Person, puis renseigner la table Authentification en hashant votre adresse mail et votre password.
Pour pouvoir hasher en sha256, vous pouvez utiliser ce site: https://md5decrypt.net/Sha256/

### Database generation
Pour utiliser l'application avec des données factices, lancer l'exécution avec pour paramètre `--DataGenParam=1`.
Pour renseigner ce paramètre sur IntelliJ, sélectionnez Edit Configuration dans le menu déroulant en haut à droite, et ajoutez le dans Program arguments.